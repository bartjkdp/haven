import PropTypes from 'prop-types'
import React from 'react'
import { GitLabLogo } from '@commonground/design-system'

import Logo from '../Logo'
import { StyledHeader, StyledContainer, StyledLogoLink, StyledGitlabLink } from './index.styles'

const Header = ({ siteTitle }) => (
  <StyledHeader>
    <StyledContainer>
      <StyledLogoLink to="/">
        <Logo width="40px" height="40px" />
        <span>{siteTitle}</span>
      </StyledLogoLink>
      <StyledGitlabLink>
        <a href="https://www.gitlab.com/commonground/haven/haven" target="_blank" rel="noopener noreferrer" aria-label="View project on GitLab">
          <GitLabLogo width="26px" height="24px" />
        </a>
      </StyledGitlabLink>
    </StyledContainer>
  </StyledHeader>
)

Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: `Haven`,
}

export default Header
