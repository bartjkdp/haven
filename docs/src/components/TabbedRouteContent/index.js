// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { string, arrayOf, shape } from 'prop-types'

import { Wrapper, Header, Content, Tabs, Tab, TabLink } from './index.styles'

const TabbedRouteContent = ({ tabs, children }) => {
  return (
    <Wrapper>
      <Header>
        <nav>
          <Tabs>
            {tabs &&
              tabs.map(({ to, title, exact }) => (
                <Tab key={title}>
                  <TabLink to={to} exact={exact} activeClassName="active">
                    {title}
                  </TabLink>
                </Tab>
              ))}
          </Tabs>
        </nav>
      </Header>

      <Content>{children}</Content>
    </Wrapper>
  )
}

TabbedRouteContent.propTypes = {
  tabs: arrayOf(
    shape({
      to: string,
      title: string,
    }),
  )
}

TabbedRouteContent.defaultProps = {
  tabs: [],
}

export default TabbedRouteContent
