import React from 'react'
import { Router } from '@reach/router'
import { useStaticQuery, graphql } from 'gatsby'


import Layout from '../components/Layout'
import SEO from '../components/SEO'
import Container from '../components/Container'
import Jumbotron from '../components/Jumbotron'
import TabbedRouteContent from '../components/TabbedRouteContent'
import CheckList from '../components/CheckList'

const checksPerCategory = (checks) => {
  let categories = {}

  checks.forEach((check) => {
    if (!categories[check.category]) {
      categories[check.category] = []
    }

    categories[check.category].push(check)
  })

  return categories
}

const StandardPage = () => {
  const { staticYaml } = useStaticQuery(
    graphql`
      query {
        staticYaml {
          compliancychecks {
            name
            label
            category
            rationale
            sshrequired
          }
          suggestedchecks {
            name
            label
            category
            rationale
            sshrequired
          }
        }
      }
    `
  )

  const tabs = [
    { to: `/standard/`, title: 'Required checks' },
    { to: `/standard/suggestions`, title: 'Suggestions' }
  ]

  return (
    <Layout>
      <SEO />
      <Jumbotron isDark textAlign="left">
        <Container>
          <h1>De standaard</h1>
          <Jumbotron.Paragraph>Haven is een standaard voor cloud-agnostische infrastructuur en schrijft een specifieke configuratie van Kubernetes voor om het geschikt te maken voor gebruik binnen overheden.</Jumbotron.Paragraph>
          <Jumbotron.Paragraph>De standaard bestaat momenteel uit {staticYaml.compliancychecks.length} verplichte en {staticYaml.suggestedchecks.length} voorgestelde checks.</Jumbotron.Paragraph>
        </Container>
    </Jumbotron>
      <Container>
        <TabbedRouteContent tabs={tabs}>
          <Router>
            <CheckList path="/standard/" checks={checksPerCategory(staticYaml.compliancychecks)} />
            <CheckList path="/standard/suggestions" checks={checksPerCategory(staticYaml.suggestedchecks)} />
          </Router>
        </TabbedRouteContent>
      </Container>
    </Layout>
  )
}

export default StandardPage
