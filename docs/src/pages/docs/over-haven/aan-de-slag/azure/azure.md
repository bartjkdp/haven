---
title: "Microsoft Azure Kubernetes Service (AKS)"
path: "/docs/aan-de-slag/azure"
---

_Referentie Implementatie: Haven op Azure met AKS_

Kies bij services voor "Kubernetes-services". Klik vervolgens op Toevoegen > Een Kubernetes-cluster toevoegen. Gebruik de volgende instellingen:

- Resourcegroep: maak een nieuwe groep aan of kies een bestaande groep
- Kubernetes-versie: kies de laatste stabiele versie
- Regio: West-Europa
- Virtuele knooppunten: Uitgeschakeld
- Virtuele-machineschaalsets: Ingeschakeld
- Verificatiemethode: Service-principal
- Op rollen gebaseerd toegangsbeheer: Ingeschakeld
- Door AKS beheerde Azure Active Directory: Ingeschakeld
- Versleutelingstype: (Standaard) Versleuteling van data-at-rest met een door het platform beheerde sleutel
- Netwerkconfiguratie: Azure CNI
- DNS-naamvoorvoegsel: Naar keuze (bijvoorbeeld: haven)
- Load balancer: Standard
- Persoonlijk cluster: Naar keuze
- Geautoriseerde IP-bereiken: Gebruik wanneer persoonlijk cluster op Uitgeschakeld staat
- Netwerkbeleid: Calico
- HTTP-toepassingsroutering: Nee
- Containerregister: Naar keuze
- Containerbewaking: Naar keuze
- Azure Policy: Naar keuze

Kies voor de knooppuntgroepen het aantal en de grootte van nodes naar keuze. Wij adviseren nodes met minimaal 2 vCPU's en 4GB RAM. Hou er rekening mee dat een node voldoende ruimte heeft voor het opslaan van de Docker images. Dit is afhankelijk van de te installeren applicaties. Overweeg ook om auto scaling te gebruiken.

Het is bij Azure CNI belangrijk om te zorgen dat [IP addressering goed gepland is](https://docs.microsoft.com/nl-nl/azure/aks/configure-azure-cni#plan-ip-addressing-for-your-cluster). Het maximum aantal pods per node kan geconfigureerd worden in de instellingen. Wij adviseren de standaardinstelling van 110 pods per node. Het standaard subnet (10.250.0.0/16) biedt ruimte voor 65.536 adressen. Hierin past een cluster tot ongeveer 550 nodes: 550 + (550*110) = 61.050.

Deze voorbeeldconfiguratie is [ook beschikbaar als Terraform code](https://gitlab.com/commonground/haven/haven/-/tree/master/reference/azure).

![Schermafbeelding van een Kubernetes-cluster op Azure Kubernetes Service (AKS)](./azure.png)
