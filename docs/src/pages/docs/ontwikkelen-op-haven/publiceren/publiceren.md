---
title: "Applicatie publiceren"
path: "/docs/applicatie-publiceren"
---

In deze sectie vind je een aantal aanwijzingen voor het publiceren van een applicatie. Door het volgen van deze aanwijzingen wordt een ontwikkelde applicatie eenvoudig installeerbaar voor anderen.

## Versiebeheer

Met [Semantic Versioning](https://semver.org/) kunnen wijzigingen aan een applicatie eenduidig gecommuniceerd worden naar gebruikers. Gebruikers kunnen hiermee in een oogopslag de aard van de wijzigingen beoordelen.

## Configureerbaar

Zorg dat gebruikers eenvoudig de applicatie naar eigen wens kunnen aanpassen met behulp van instellingen, zodat een gebruiker geen wijzigingen hoeft te maken in de broncode van de applicatie. Dit zorgt er ook voor dat ontwikkeling van de applicatie op één plek kan plaatsvinden. Je kunt de applicatie instelbaar maken met bijvoorbeeld environment variabelen, een configuratiebestand of instellingen in een database.

## Publiceer Docker images

Door kant-en-klare Docker images voor de applicatie te publiceren op een publiek Docker register, wordt het voor gebruikers eenvoudig om de applicatie te gebruiken. Hiermee voorkom je dat gebruikers zelf de applicatie moeten bouwen en hier pipelines voor moeten inrichten.

## Publiceer Helm charts

Publiceer een [Helm chart](https://helm.sh/) voor de applicatie zodat de ontwikkelde applicatie met één commando uitgerold kan worden op een Haven cluster. Hou bij het ontwikkelen van de Helm chart zoveel mogelijk de [standaard Helm template](https://helm.sh/docs/topics/charts/#using-helm-to-manage-charts) aan.

## Maak een verwijzing op de Common Ground Componentencatalogus

Publiceer de applicatie op [componentencatalogus.commonground.nl](https://componentencatalogus.commonground.nl) zodat andere overheidsorganisaties de applicatie eenvoudig kunnen vinden.

&ensp;

---

*De ontwikkelen op Haven pagina's dienen ter illustratie en staan los van Haven Compliancy*
