---
title: "Extern publiceren"
path: "/docs/extern-publiceren"
---

In de vorige sectie hebben we een applicatie binnen het cluster beschikbaar gemaakt. In deze sectie laten we zien hoe je deze applicatie extern beschikbaar maakt. Dit kan zowel via een Ingress als met een LoadBalancer service.

## Ingress

De eenvoudigste manier om een HTTP service extern beschikbaar te maken is via een Ingress. Het externe verkeer loopt dan via een centrale load balancer. Deze load balancer zorgt voor SSL offloading en beheert automatisch de SSL certificaten via bijvoorbeeld [Let's Encrypt](https://letsencrypt.org/). Het is ook mogelijk om eigen SSL certificaten te configureren.

Om de hallo-wereld applicatie via een Ingress beschikbaar te maken dien je allereerst te beschikken over een domeinnaam. De Ingress controller bepaalt namelijk op basis van de hostname naar welke service het verkeer gestuurd wordt.

Maak allereerst op de domeinnaam een CNAME record aan voor de applicatie. Dit record dient te verwijzen naar het adres van de Ingress controller van het cluster. Dit adres kun je bij de beheerder opvragen.

```
hallo-wereld.mijn-applicatie.nl CNAME ingress-controller.het-cluster.nl.
```

Maak vervolgens met een editor het volgende bestand aan:

*hallo-wereld-ingress.yaml*

```yaml
apiVersion: networking.k8s.io/v1beta1
kind: Ingress
metadata:
  name: hallo-wereld
  namespace: default
  annotations:
    cert-manager.io/cluster-issuer: letsencrypt-prod
spec:
  tls:
    - hosts:
        - hallo-wereld.mijn-applicatie.nl
      secretName: hallo-wereld-tls
  rules:
    - host: hallo-wereld.mijn-applicatie.nl
      http:
        paths:
          - path: /
            backend:
              serviceName: hallo-wereld
              servicePort: 80
```

Pas in dit bestand `hallo-wereld.mijn-applicatie.nl` aan naar de domeinnaam die je hierboven voor de applicatie hebt aangemaakt.

Maak vervolgens de Ingress aan met:

```bash
$ kubectl apply -f hallo-wereld-ingress.yaml
```

De Ingress controller vraagt nu automatisch een SSL certificaat aan bij [Let's Encrypt](https://letsencrypt.org/). De applicatie is na een minuut beschikbaar op de domeinnaam die je hebt geconfigureerd.

Meer informatie over Ingress vind je op [Kubernetes.io](https://kubernetes.io/docs/concepts/services-networking/ingress/). Hier vind je onder andere hoe je eigen SSL certificaten kunt configureren.

## LoadBalancer service

Je hebt ook de mogelijkheid om een service beschikbaar te maken via een eigen extern IP adres door middel van een LoadBalancer service. Dit kan bijvoorbeeld handig zijn wanneer de service geen HTTP ondersteunt. Gebruik hiervoor de volgende service configuratie:

*hallo-wereld-service.yaml*
```yaml
apiVersion: v1
kind: Service
metadata:
  name: hallo-wereld
  namespace: default
spec:
  type: LoadBalancer
  selector:
    app: hallo-wereld
  ports:
    - protocol: TCP
      port: 80
      targetPort: 80
```

Aan deze configuratie is het veld `type: LoadBalancer` toegevoegd. Pas vervolgens de service configuratie aan met:

```bash
$ kubectl apply -f hallo-wereld-service.yaml
```

Je kunt vervolgens met het volgende commando de status van de service bekijken:

```bash
$ kubectl get services
NAME           TYPE           CLUSTER-IP       EXTERNAL-IP   PORT(S)        AGE
hallo-wereld   LoadBalancer   100.70.192.153   <pending>     80:31664/TCP   3s
```

Na ongeveer één minuut wordt er automatisch een IP adres toegekend aan de service:

```bash
$ kubectl get services
NAME           TYPE           CLUSTER-IP       EXTERNAL-IP       PORT(S)        AGE
hallo-wereld   LoadBalancer   100.70.192.153   141.105.122.123   80:31664/TCP   108s
```

Je kunt de service vervolgens extern bereiken via het IP adres dat zichtbaar is onder EXTERNAL-IP.

Meer informatie over de LoadBalancer service kun je vinden op [Kubernetes.io](https://kubernetes.io/docs/concepts/services-networking/service/#loadbalancer).

![Schermafbeelding van de applicatie bereikbaar via de LoadBalancer service](./loadbalancer-ip-browser.png)

&ensp;

---

*De ontwikkelen op Haven pagina's dienen ter illustratie en staan los van Haven Compliancy*
