---
title: "Persistente volumes"
path: "/docs/persistente-volumes"
---

Applicaties op een Haven cluster draaien binnen [Docker containers](https://nl.wikipedia.org/wiki/Docker_(software)). Deze containers zijn standaard *stateless*. Dit betekent dat een wijziging aan de container slechts van tijdelijke aard is. Op het moment dat een container opnieuw opgestart wordt (wat op elk moment kan gebeuren) worden niet-opgeslagen wijzigingen automatisch ongedaan gemaakt.

Vanuit beheer en beveiliging is dit een zeer wenselijke eigenschap. Dit zorgt er namelijk voor dat alle wijzigingen die er plaatsvinden vastgelegd zijn in de configuratie van het cluster of in een Docker image.

Voor toepassingen waarbij het nodig is om data op te slaan, bijvoorbeeld databases en gebruikersdata, biedt Haven persistente volumes. Bepaalde paden binnen een container worden dan persistent. Het cluster zorgt er automatisch voor dat het persistente volume beschikbaar is op het moment dat een container op een andere plek opstart.

Er zijn twee typen persistente volumes: ReadWriteOnce en ReadWriteMany. Een volume van het type ReadWriteOnce kan door maximaal één container tegelijk gebruikt worden. Volumes van het type ReadWriteMany kunnen door meerdere containers tegelijk gebruikt worden.

## Persistent volume claim

Het aanmaken van een persistent volume begint met een claim. Hiermee wordt een verzoek gedaan aan het cluster om het volume aan te maken:

*hallo-wereld-pvc.yaml*
```yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: hallo-wereld
  namespace: default
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 8Gi
```

In de claim geef je het type storage en de grootte aan. Maak vervolgens de claim aan met:

```bash
kubectl apply -f hallo-wereld-pvc.yaml
```

Vervolgens kan de status van de claim bekeken worden met:

```bash
$ kubectl get pvc
NAME           STATUS    VOLUME   CAPACITY   ACCESS MODES   STORAGECLASS   AGE
hallo-wereld   Pending                                      cinder         5s
```

Na ongeveer een minuut is het volume beschikbaar:

```bash
$ kubectl get pvc
NAME           STATUS   VOLUME                                     CAPACITY   ACCESS MODES   STORAGECLASS   AGE
hallo-wereld   Bound    pvc-dfe5b951-dd07-49f4-a721-eb8543043d2a   8Gi        RWO            cinder         69s
```

Het volume kan vervolgens in een deployment gebruikt worden. Pas hiervoor de deployment aan:

*hallo-wereld-deployment.yaml*
```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: hallo-wereld
  namespace: default
  labels:
    app: hallo-wereld
spec:
  replicas: 1
  selector:
    matchLabels:
      app: hallo-wereld
  template:
    metadata:
      labels:
        app: hallo-wereld
    spec:
      containers:
        - name: hallo-wereld
          image: registry.gitlab.com/commonground/haven/hallo-wereld:1.0
          ports:
            - containerPort: 80
          volumeMounts:
            - name: hallo-wereld
              mountPath: /srv/storage
      volumes:
        - name: hallo-wereld
          persistentVolumeClaim:
            claimName: hallo-wereld
```

In de deployment is het onderdeel `volumes:` en `volumeMounts:` toegevoegd. Met volumes wordt de naam van de claim gekoppeld aan een naam van het volume in de deployment. Vervolgens deze naam met volumeMounts gekoppeld aan een specifiek pad in de container, in dit geval kiezen we voor `/srv/storage`.

Pas de wijzigingen in de deployment toe met:

```bash
$ kubectl apply -f hallo-wereld-deployment.yaml
```

We kunnen vervolgens inloggen op de container van de deployment om te kijken of het volume daadwerkelijk is aangemaakt. Gebruik hiervoor:

```bash
$ kubectl exec -it deploy/hallo-wereld sh
```

We bekijken vervolgens de volumes die we beschikbaar hebben:

```bash
/ $ df -h
Filesystem                Size      Used Available Use% Mounted on
overlay                  14.7G     10.9G      3.1G  78% /
tmpfs                    64.0M         0     64.0M   0% /dev
tmpfs                     1.9G         0      1.9G   0% /sys/fs/cgroup
/dev/vde                  7.8G     36.0M      7.8G   0% /srv/storage
```

We hebben het volume van 8GB beschikbaar onder /srv/storage.

&ensp;

---

*De ontwikkelen op Haven pagina's dienen ter illustratie en staan los van Haven Compliancy*
