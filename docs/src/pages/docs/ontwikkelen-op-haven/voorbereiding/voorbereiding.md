---
title: "Voorbereiding"
path: "/docs/voorbereiding"
---

Voordat je aan de slag kunt gaan heb je eerst toegang nodig tot een Haven cluster. De beheerder geeft je toegang en een link waarmee je kan inloggen. Zorg verder dat je [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/) op je computer hebt geïnstalleerd voordat je verder gaat.

## Kubectl

Met kubectl kun je toegang krijgen tot het cluster via de command line.

1. Ga naar de inloglink die je van de beheerder hebt gekregen.
1. Log in met je account, daarna kom je terug op een pagina met instructies.
1. Voer de instructies uit om kubectl te configureren met het cluster.

Je kunt nu met kubectl verbinding maken met het cluster, probeer bijvoorbeeld maar eens het volgende om een lijst van alle nodes in het cluster te zien:

```bash
$ kubectl top nodes
```

![Schermafbeelding van het commando kubectl top nodes](./kubectl-top-nodes.png)

## Kubernetes Dashboard

Het Kubernetes dashboard geeft je toegang tot het cluster via een grafische interface.

1. Volg eerst de instructies hierboven om kubectl te configureren.
1. Draai vervolgens het volgende commando om een proxy op te zetten naar het cluster:

    ```bash
    $ kubectl proxy
    ```

1. Ga op je lokale machine naar de volgende link: http://localhost:8001/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy.

![Schermafbeelding van het Kubernetes dashboard](./kubernetes-dashboard.png)

Je weet nu hoe je met kubectl verbinding kunt maken met het cluster en hoe je het Kubernetes dashboard kunt gebruiken om een grafische weergave van het cluster te krijgen. In de <a href="/docs/hallo-wereld">volgende stap</a> rollen we een nieuwe applicatie uit op het cluster.

&ensp;

---

*De ontwikkelen op Haven pagina's dienen ter illustratie en staan los van Haven Compliancy*
