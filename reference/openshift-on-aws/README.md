# Reference Implementation: Haven on AWS

## OpenShift on AWS
Haven can easily be deployed on AWS using [OpenShift](https://www.openshift.com).

## How it works
Follow the steps from the [online documentation](https://haven.commonground.nl/docs/aan-de-slag/openshift-op-aws). An example machineset.yaml file is available in this directory to make installation of OpenShift Container Storage straightforward.

When ready ensure your new cluster is Haven Compliant by using the [Haven Compliancy Checker](../../haven/cli/README.md).

## Addons
Please look into the [Haven CLI](../../haven/cli/README.md) for addons management.

## License
Copyright © VNG Realisatie 2019-2021
Licensed under the EUPL
