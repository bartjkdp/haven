# Reference Implementation: Haven on Azure

## AKS
Haven can easily be deployed on Azure using [AKS](https://docs.microsoft.com/azure/aks).

## How it works
Follow the steps from the [online documentation](https://haven.commonground.nl/docs/aan-de-slag/azure) or have a look at the [terraform code](./).

When ready ensure your new cluster is Haven Compliant by using the [Haven Compliancy Checker](../../haven/cli/README.md).

## Addons
Please look into the [Haven CLI](../../haven/cli/README.md) for addons management.

## License
Copyright © VNG Realisatie 2019-2021
Licensed under the EUPL
