# Addons

## Legacy
These are the legacy addons which are tied to the OpenStack Reference Implementation and will be removed when all of them have been ported to the Haven CLI.

For the current addon approach using the Haven CLI, see: [../../../haven/cli/README.md](../../../haven/cli/README.md).

### Haven Dashboard
See [haven-dashboard/README.md](haven-dashboard/README.md) for instructions on how to deploy the Haven Dashboard.

The implementation of the dashboard can be found at [../../../haven/dashboard/README.md](../../../haven/dashboard/README.md).

### Kubernetes dashboard
See [kubernetes-dashboard/README.md](kubernetes-dashboard/README.md) for instructions on how to deploy the Kubernetes dashboard.

### Traefik
See [traefik/README.md](traefik/README.md) for instructions on how to deploy a Traefik ingress controller and cert-manager for automated SSL certificate management.

### Monitoring
See [monitoring/README.md](monitoring/README.md) for instructions on how to deploy monitoring with Loki, Prometheus, Netchecker and Grafana.

### OpenID Connect
See [oidc/README.md](oidc/README.md) for instructions on how to deploy OpenID Connect powered by Dex, allowing for Active Directory/Github etc. authentication.

## License
Copyright © VNG Realisatie 2019-2021<br />
Licensed under EUPL v1.2
