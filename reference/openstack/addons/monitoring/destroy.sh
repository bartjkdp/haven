#!/bin/bash

# Copyright © VNG Realisatie 2019-2021
# Licensed under the EUPL

set -e

echo -ne "[Haven] This will DESTROY Loki, Prometheus, Grafana and Netchecker on your currently active Haven cluster.\n\nContinue [y/n] "; read CONTINUE; echo; [[ "y" != $CONTINUE ]] && (echo "ABORTED"; exit 1)

MONITORING_DEPLOYPATH=$STATEPATH/addons/monitoring
OIDC_DEPLOYPATH=$STATEPATH/addons/oidc

if [[ ! -d $MONITORING_DEPLOYPATH ]]; then
    echo "$MONITORING_DEPLOYPATH does not exist, nothing to destroy!"

    exit 1
fi

# Loki.
helm uninstall -n monitoring loki

# Prometheus and Grafana.
helm uninstall -n monitoring prometheus

kubectl -n monitoring delete -f $MONITORING_DEPLOYPATH/discover.yaml
kubectl -n monitoring delete pvc alertmanager-prometheus-kube-prometheus-alertmanager-db-alertmanager-prometheus-kube-prometheus-alertmanager-0
kubectl -n monitoring delete pvc prometheus-prometheus-kube-prometheus-prometheus-db-prometheus-prometheus-kube-prometheus-prometheus-0

# Netchecker.
kubectl delete -f $MONITORING_DEPLOYPATH/netchecker-agent-sa.yaml
kubectl delete -f $MONITORING_DEPLOYPATH/netchecker-agent-ds.yaml
kubectl delete -f $MONITORING_DEPLOYPATH/netchecker-agent-hostnet-clusterrole.yaml
kubectl delete -f $MONITORING_DEPLOYPATH/netchecker-agent-hostnet-clusterrolebinding.yaml
kubectl delete -f $MONITORING_DEPLOYPATH/netchecker-agent-hostnet-ds.yaml
kubectl delete -f $MONITORING_DEPLOYPATH/netchecker-agent-hostnet-psp.yaml
kubectl delete -f $MONITORING_DEPLOYPATH/netchecker-server-sa.yaml
kubectl delete -f $MONITORING_DEPLOYPATH/netchecker-server-clusterrole.yaml
kubectl delete -f $MONITORING_DEPLOYPATH/netchecker-server-clusterrolebinding.yaml
kubectl delete -f $MONITORING_DEPLOYPATH/netchecker-server-deployment.yaml
kubectl delete -f $MONITORING_DEPLOYPATH/netchecker-server-svc.yaml

# OIDC cleanup.
echo -e "\n\n[Haven] Cleaning up OIDC client list. Please confirm the upgrade."

awk '/id\: grafana/{while(getline && $0 != "" && index($0, "id:") == 0){}}1' $OIDC_DEPLOYPATH/dex-values.yaml > /tmp/dex-values.yaml && mv /tmp/dex-values.yaml $OIDC_DEPLOYPATH/dex-values.yaml

bash /opt/haven/addons/oidc/upgrade.sh

# Wrap up.
rm -r $MONITORING_DEPLOYPATH

echo -e "\n\n[Haven] Destroyed Loki, Prometheus, Grafana and Netchecker.\n\nIf your monitoring namespace is now empty, you are free to delete it.\n\n"
