#!/bin/bash

# Copyright © VNG Realisatie 2019-2021
# Licensed under the EUPL

set -e

echo -ne "[Haven] This will upgrade Loki, Prometheus, Grafana and Netchecker on your currently active Haven cluster.\n\nContinue [y/n] "; read CONTINUE; echo; [[ "y" != $CONTINUE ]] && (echo "ABORTED"; exit 1)

DEPLOYPATH=$STATEPATH/addons/monitoring

# Loki.
helm repo add loki https://grafana.github.io/loki/charts
helm repo update

helm upgrade loki --namespace monitoring loki/loki-stack -f $DEPLOYPATH/values-loki.yaml

# Prometheus / Grafana.
sed "s/%GRAFANA_PASS%/$(getsecret monitoring_grafana_pass)/g" $DEPLOYPATH/values-prometheus.yaml > /tmp/values-prometheus.yaml

helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo update

helm upgrade prometheus --namespace monitoring prometheus-community/kube-prometheus-stack --set prometheusOperator.createCustomResource=false -f /tmp/values-prometheus.yaml

rm -f /tmp/values-prometheus.yaml

# Netchecker.
kubectl apply -f $DEPLOYPATH/netchecker-agent-sa.yaml
kubectl apply -f $DEPLOYPATH/netchecker-agent-ds.yaml
kubectl apply -f $DEPLOYPATH/netchecker-agent-hostnet-clusterrole.yaml
kubectl apply -f $DEPLOYPATH/netchecker-agent-hostnet-clusterrolebinding.yaml
kubectl apply -f $DEPLOYPATH/netchecker-agent-hostnet-ds.yaml
kubectl apply -f $DEPLOYPATH/netchecker-agent-hostnet-psp.yaml
kubectl apply -f $DEPLOYPATH/netchecker-server-sa.yaml
kubectl apply -f $DEPLOYPATH/netchecker-server-clusterrole.yaml
kubectl apply -f $DEPLOYPATH/netchecker-server-clusterrolebinding.yaml
kubectl apply -f $DEPLOYPATH/netchecker-server-deployment.yaml
kubectl apply -f $DEPLOYPATH/netchecker-server-svc.yaml

## Done.
echo -e "\n\n[Haven] Upgraded Loki, Prometheus, Grafana and Netchecker.\n\n"
