# Haven - Upgrading OpenID Connect stack

## Upgrading OIDC

You can upgrade the OIDC stack (dex + dex-k8s-authenticator) by running `addons/oidc/upgrade.sh`.

Version numbers for the stack components are assumed by the Haven version you are using when running the upgrade.

Should be a painless upgrade. Feel free to also use this script to apply settings changes through values files.

## License
Copyright © VNG Realisatie 2019-2021
Licensed under the EUPL
