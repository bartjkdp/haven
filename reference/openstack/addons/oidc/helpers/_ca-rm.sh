#!/bin/bash

# Copyright © VNG Realisatie 2019-2021
# Licensed under the EUPL

# HELPER SCRIPT. SHOULD NOT BE RUN MANUALLY.

set -e

echo "Cleaning up Kubernetes CA."

rm -f /home/core/k8s-ca.crt
