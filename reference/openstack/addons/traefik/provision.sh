#!/bin/bash

# Copyright © VNG Realisatie 2019-2021
# Licensed under the EUPL

set -e

echo -ne "[Haven] This will provision Traefik and cert-manager on your currently active Haven cluster.\n\nContinue [y/n] "; read CONTINUE; echo; [[ "y" != $CONTINUE ]] && (echo "ABORTED"; exit 1)

DEPLOYPATH=$STATEPATH/addons/traefik

if [[ ! -d $DEPLOYPATH ]]; then
    mkdir -p $DEPLOYPATH
fi

# Create ns.
echo "[Haven] Creating namespace 'traefik' and 'cert-manager' if it does not yet exist."
kubectl create ns traefik || echo "Assuming namespace traefik already exists."
kubectl create ns cert-manager || echo "Assuming namespace cert-manager already exists."

echo "[Haven] Labeling namespace for network policies."
kubectl label ns traefik --overwrite networkPolicy=ingress

# Copy dist files to non-existing deploy files.
echo -e "\n[Haven] Copying distribution files to state directory without overwriting existing files."

false | cp -i /opt/haven/addons/traefik/dist/* $DEPLOYPATH/

# Install Traefik.
echo -e "\n\n[Haven] Deploying Traefik."

helm repo add traefik https://containous.github.io/traefik-helm-chart
helm repo update

helm upgrade --install \
    traefik-$CLUSTER traefik/traefik \
    --namespace traefik \
    --values $DEPLOYPATH/traefik-values.yml

# Traefik was not yet installed before, so ssl-configuration must be applied afterwards for 'TLSOption' to be known. Requires traefik restart (configmap).
kubectl apply -f $DEPLOYPATH/ssl-configuration.yml
kubectl -n traefik rollout restart deployment traefik-$CLUSTER

# Install cert-manager.
echo -e "\n\n[Haven] Deploying cert-manager."

helm repo add jetstack https://charts.jetstack.io
helm repo update

kubectl apply -f https://github.com/jetstack/cert-manager/releases/download/v${CERT_MANAGER_VERSION}/cert-manager.crds.yaml

helm upgrade --install \
  cert-manager jetstack/cert-manager \
  --namespace cert-manager \
  --version "v${CERT_MANAGER_CHART_VERSION}"

# Wait till webhook is ready.
while ! kubectl -n cert-manager get pods --selector=app.kubernetes.io/instance=cert-manager,app=webhook | grep "1\/1" >/dev/null; do echo "[Haven] $(date) Waiting 10s for cert-manager-webhook to become healthy"; sleep 10; done

# Sleep some more.
echo "[Haven] $(date) Waiting 30s for cert-manager-webhook to really get ready"; sleep 30;

# Create Let's Encrypt ClusterIssuer.
kubectl apply -f $DEPLOYPATH/letsencrypt-prod.yml

## Done.
echo -e "\n[Haven] Deployed Traefik and cert-manager.\n"
