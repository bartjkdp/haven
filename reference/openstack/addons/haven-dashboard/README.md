# Haven - dashboard

This addon installs the Haven dashboard and exposes the dashboard through an ingress. It will also install the Dex authentication provider that will be used for protecting the dashboard. During installation an admin password will be generated.

## Prerequisites

Make sure you installed the [Traefik addon](../traefik/README.md).

## Install

From Haven run `addons/haven-dashboard/provision.sh`. The script will guide you through what you have to do to prepare for a deployment. Make sure provided hostnames have their DNS resolve to the ingress controller.

To get the status of the deployment run `addons/haven-dashboard/status.sh`

## Cleanup

From Haven run `addons/haven-dashboard/destroy.sh`.

## License
Copyright © VNG Realisatie 2019-2021
Licensed under the EUPL
