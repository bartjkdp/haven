#!/bin/bash

# Copyright © VNG Realisatie 2019-2021
# Licensed under the EUPL

set -e

echo -ne "[Haven] This will provision Haven dashboard on your currently active Haven cluster.\n\nContinue [y/n] "; read CONTINUE; echo; [[ "y" != $CONTINUE ]] && (echo "ABORTED"; exit 1)

DEPLOYPATH=$STATEPATH/addons/haven-dashboard

if [[ ! -d $DEPLOYPATH ]]; then
    mkdir -p $DEPLOYPATH
fi

if [[ ! -f $DEPLOYPATH/dashboard.host ]]; then
    echo "[Haven] First create $DEPLOYPATH/dashboard.host (see dist/ for an example)."

    exit 1
fi

# Create ns.
echo "[Haven] Creating namespace 'haven-dashboard' if it does not yet exist."
kubectl create ns haven-dashboard || echo "Assuming namespace haven-dashboard already exists."

# Copy dist files to non-existing deploy files.
echo -e "\n[Haven] Copying haven-dashboard distribution files to state directory without overwriting existing files."

false | cp -i /opt/haven/addons/haven-dashboard/dist/* $DEPLOYPATH/

# Patch config files.
DASHBOARD_HOST=$(cat $DEPLOYPATH/dashboard.host)

echo -e "\n\n[Haven] Patching config files with variables."

sed -i "s|%DASHBOARD_HOST%|${DASHBOARD_HOST}|g" $DEPLOYPATH/dex-values.yaml
sed -i "s|%DASHBOARD_HOST%|${DASHBOARD_HOST}|g" $DEPLOYPATH/dashboard-values.yaml

# Generate secrets.
ADMIN_PASSWORD=$(< /dev/urandom tr -dc _A-Z-a-z-0-9 | head -c20)
OIDC_CLIENT_SECRET=$(< /dev/urandom tr -dc _A-Z-a-z-0-9 | head -c20)
setsecret haven_dashboard_admin_password $ADMIN_PASSWORD
setsecret haven_dashboard_oidc_client_secret $OIDC_CLIENT_SECRET

# Deploy Helm charts.
cp $DEPLOYPATH/dex-values.yaml /tmp/dex-values.yaml
ADMIN_PASSWORD_HASH=$(htpasswd -bnBC 10 "" ${ADMIN_PASSWORD} | cut -d ':' -f 2)
sed -i "s|%ADMIN_PASSWORD_HASH%|${ADMIN_PASSWORD_HASH}|g" /tmp/dex-values.yaml
sed -i "s|%OIDC_CLIENT_SECRET%|${OIDC_CLIENT_SECRET}|g" /tmp/dex-values.yaml

helm repo add commonground https://charts.commonground.nl/
helm repo update

helm install dex commonground/dex \
    --namespace haven-dashboard \
    --values /tmp/dex-values.yaml

rm /tmp/dex-values.yaml

cp $DEPLOYPATH/dashboard-values.yaml /tmp/dashboard-values.yaml
sed -i "s|%OIDC_CLIENT_SECRET%|${OIDC_CLIENT_SECRET}|g" /tmp/dashboard-values.yaml
sed -i "s|%SECRET_KEY%|$(< /dev/urandom tr -dc _A-Z-a-z-0-9 | head -c32)|g" /tmp/dashboard-values.yaml

helm install haven-dashboard commonground/haven-dashboard \
    --namespace haven-dashboard \
    --values /tmp/dashboard-values.yaml

rm /tmp/dashboard-values.yaml

## Done.
echo -e "\n\n[Haven] ** Haven dashboard email: admin@haven.vng.cloud password: $ADMIN_PASSWORD **"

echo -e "\n[Haven] Deployed Haven dashboard.\n"
