#!/usr/bin/env bash

# Copyright © VNG Realisatie 2019-2021
# Licensed under the EUPL

set -e

echo -ne "[Haven] This will provision Open Policy Agent Gatekeeper on your currently active Haven cluster.\n\nContinue [y/n] "; read CONTINUE; echo; [[ "y" != $CONTINUE ]] && (echo "ABORTED"; exit 1)

echo -e "\n[Haven] Deploying chart"

DEPLOYPATH=$STATEPATH/addons/open-policy-agent

if [[ ! -d $DEPLOYPATH ]]; then
    mkdir -p $DEPLOYPATH
fi

false | cp -i /opt/haven/addons/open-policy-agent/dist/* $DEPLOYPATH/

helm repo add gatekeeper https://open-policy-agent.github.io/gatekeeper/charts
helm upgrade --install gatekeeper gatekeeper/gatekeeper -f $DEPLOYPATH/values.yaml

echo -ne "\n[Haven] Do you want to configure the default Haven policies on your cluster? [y/n] "; read CONTINUE

if [[ "$CONTINUE" == "y" ]]; then
    kubectl apply -f /opt/haven/addons/open-policy-agent/resources/config.yaml
    kubectl apply -f /opt/haven/addons/open-policy-agent/resources/constraint-templates

    for i in 1 2 3 4 5; do kubectl wait --for condition=established --timeout=60s \
        crd/k8sblocknodeport.constraints.gatekeeper.sh \
        crd/k8scontainerlimits.constraints.gatekeeper.sh \
        crd/k8spspprivilegedcontainer.constraints.gatekeeper.sh && break || sleep 10; done

    kubectl apply -f /opt/haven/addons/open-policy-agent/resources/constraints
fi

## Done.
echo -e "\n\n[Haven] Deployed Open Policy Agent Gatekeeper.\n"
