#!/bin/bash

# Copyright © VNG Realisatie 2019-2021
# Licensed under the EUPL

echo -ne "[Haven] This will remove Open Policy Agent Gatekeeper.\n\nContinue [y/n] "; read CONTINUE; echo; [[ "y" != $CONTINUE ]] && (echo "ABORTED"; exit 1)

DEPLOYPATH=$STATEPATH/addons/open-policy-agent

helm uninstall gatekeeper

kubectl delete -f /opt/haven/addons/open-policy-agent/resources/config.yaml
kubectl delete -f /opt/haven/addons/open-policy-agent/resources/constraint-templates
kubectl delete -f /opt/haven/addons/open-policy-agent/resources/constraints

kubectl delete crd \
  configs.config.gatekeeper.sh \
  constraintpodstatuses.status.gatekeeper.sh \
  constrainttemplatepodstatuses.status.gatekeeper.sh \
  constrainttemplates.templates.gatekeeper.sh

# Wrap up.
rm -r $DEPLOYPATH

echo -e "\n\n[Haven] Open Policy Agent Gatekeeper removed.\n\n"
