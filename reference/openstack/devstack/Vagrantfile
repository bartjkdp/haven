# Copyright © VNG Realisatie 2019-2021
# Licensed under the EUPL

####################################
# Haven - Development environment. #
####################################

Vagrant.configure("2") do |config|
  config.vm.define     "haven"
  config.vm.hostname = "haven"
  # XXX: Ubuntu 16.04LTS is best supported by OpenStack. Ubuntu 18.04LTS has multiple cinder issues.
  config.vm.box      = "ubuntu/xenial64"

  # Speeds up bootstrapping your vm up by caching package downloads.
  # `vagrant plugin install vagrant-cachier`.
  if Vagrant.has_plugin?("vagrant-cachier")
    config.cache.scope = :box
  end

  # Resizes disk to something more usable. Actual small image file on disk expands-on-use.
  # `vagrant plugin install vagrant-disksize`.
  if Vagrant.has_plugin?("vagrant-disksize")
    config.disksize.size = '50GB'
  end

  config.vm.provider "virtualbox" do |v|
    v.name   = "haven"
    v.memory = 14336  # Minimal 6144, preferred 8192 for OpenStack to become healthy. With Octavia enabled (default) you need 10240. Haven cluster deployment makes about 14336.
    v.cpus   = 4      # At least 2 to do anything useful. Haven cluster deployment makes 4.

    v.customize ["modifyvm", :id, "--nested-hw-virt", "on"]
  end

  # Host to OpenStack networking.
  config.vm.network "private_network", ip: "10.0.10.2"
  # Neutron private-network: Instance to Instance.
  config.vm.network "private_network", ip: "10.0.100.2", auto_config: false
  # Neutron public-network: Host + OpenStack to Instances. Requires plugin or manually adding host route (see README.md).
  config.vm.network "private_network", ip: "10.0.200.2", auto_config: false, virtualbox__intnet: true

  # Note: this plugin only supports macOS. Running Linux on your host means you have to add this route manually.
  if Vagrant.has_plugin?("vagrant-host-route")
    config.vm.provision :host_route do |host_route|
      host_route.destination = "10.0.200.0/24"
      host_route.gateway = "10.0.10.2"
    end
  end
 
  config.vm.synced_folder '.', '/vagrant', SharedFoldersEnableSymlinksCreate: false

  config.vm.provision "shell", privileged: false, path:   'bootstrap-pre.sh'
  config.vm.provision "shell", privileged: false, inline: '/home/vagrant/devstack/stack.sh', run: "always"
  config.vm.provision "shell", privileged: false, path:   'bootstrap-post.sh'
end
