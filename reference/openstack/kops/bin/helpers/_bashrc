# Copyright © VNG Realisatie 2019-2021
# Licensed under the EUPL

alias l="ls -al"
export EDITOR=vim

source /etc/profile.d/bash_completion.sh

function version { echo "$@" | sed 's/^v//' | awk -F. '{ printf("%d%03d%03d%03d\n", $1,$2,$3,$4); }'; }
export -f version

function getsecret { ansible localhost -m debug -a var="$1" -e "@${STATEPATH}/secrets.yaml" --vault-password-file $KEYFILE 2>/dev/null | tail -n +2 | head -n -1 | awk '{ print $2 }' | tr -d '"'; }
export -f getsecret

function setsecret { echo -n $2 | ansible-vault encrypt_string --vault-password-file $KEYFILE --stdin-name $1 >> $STATEPATH/secrets.yaml; }
export -f setsecret

function checkforupgrades {
    CLUSTER_VERSION=$(cat $STATEPATH/VERSION)
    UPGRADE=false

    # Do not automatically offer upgrades running latest.
    if [[ $HAVEN_VERSION != "undefined" ]]; then
        # Version check current cluster with current Haven image, dropping leading 'v' from version number.
        if [[ $(version $CLUSTER_VERSION) -lt $(version $HAVEN_VERSION) ]]; then
            UPGRADE=true
        fi
    fi

    if $UPGRADE; then
        echo "[Haven] Upgrade available. Cluster '$CLUSTER' == $CLUSTER_VERSION, but you are using Haven == $HAVEN_VERSION. Upgrade with command \"upgrade\"."
    fi
}

function help {
    echo -e "\n$(printf '%.0s-' {1..110})\n  Welcome to the Haven Reference Implementation for OpenStack - Version: $HAVEN_VERSION - Cluster: $CLUSTER.\n$(printf '%.0s-' {1..110})"

__help="
Try 'help'. Suggestions:

# Haven CLI.
- 'haven check'  # Runs the Haven Compliancy Checker on the existing cluster.
- 'haven addons install monitoring'  # Installs monitoring addon on an existing cluster.

# Reference Implementation: OpenStack.
- 'status', 'create', 'upgrade', 'destroy', 'pool'  # Cluster maintenance.
- 'openstack loadbalancer list', 'neutron lbaas-loadbalancer-list', 'ssh bastion'  # OpenStack tooling.

# Kubernetes tooling.
- 'kops validate cluster', 'kubectl top pods', 'helm list'

# Legacy addons (please use the Haven CLI mentioned earlier to manage addons).
- 'addons/<traefik|oidc|monitoring|haven-dashboard|kubernetes-dashboard|open-policy-agent>/provision.sh'

"

    echo "$__help"
}

checkforupgrades
help
