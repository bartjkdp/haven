#!/bin/bash

# Copyright © VNG Realisatie 2019-2021
# Licensed under the EUPL

set -e

cd /opt/haven

echo -ne "[Haven] About to upgrade cluster '$CLUSTER'.\n\nContinue [y/n] "; read CONTINUE; echo; [[ "y" != $CONTINUE ]] && (echo "ABORTED"; exit 1)

# Init.
source $STATEPATH/cluster.env

# Backup cluster spec.
echo "[Haven] Backing up cluster spec before making any changes."
kops get -o yaml > $STATEPATH/kops-${CLUSTER}-$(date -u '+%Y%m%d%H%I%S').yaml

# OS updates. OpenStack Debian and CoreOS images have automatic updates enabled by default.
echo "[Haven] Master and worker nodes are on an automated OS update schedule. No need to manually upgrade packages."

# Update cluster.
echo -e "[Haven] Dry running 'kops update cluster'.\n"
kops update cluster

echo -ne "\n[Haven] Apply proposed cluster changes? [y/n] "; read CONTINUE

if [[ "y" == "$CONTINUE" ]]; then
    echo -e "[Haven] Running 'kops update cluster --yes'.\n"

    kops update cluster --yes

    # Rolling-update cluster.
    echo -ne "\n[Haven] Cluster update requires a rolling-update. Continue [y/n] "; read CONTINUE

    if [[ "y" == "$CONTINUE" ]]; then
        if [[ "openstack" == $CLOUD ]]; then
            # OpenStack tends to need some more time to validate than 15 minutes.
            kops rolling-update cluster --yes --force --validation-timeout=25m
        else
            kops rolling-update cluster --yes --force
        fi
    fi
fi

# Addons: metrics-server.
echo -ne "\n[Haven] Addons: about to upgrade metrics-server. Continue [y/n] "; read CONTINUE

if [[ "y" == "$CONTINUE" ]]; then
    helm upgrade metrics-server -n kube-system --version $METRICS_CHART_VERSION --set args={"--kubelet-insecure-tls=true, --kubelet-preferred-address-types=InternalIP"} stable/metrics-server || echo "[Haven] NON-FATAL ERROR: metrics-server upgrade failed."
fi

# Done.
echo "[Haven] Bumping version."
echo $HAVEN_VERSION > $STATEPATH/VERSION

echo "[Haven] Backing up cluster spec again after the upgrade."
kops get -o yaml > $STATEPATH/kops-${CLUSTER}-$(date -u '+%Y%m%d%H%I%S').yaml

echo -e "\n[Haven] Updated cluster '$CLUSTER'.\n"
