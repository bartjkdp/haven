#!/bin/bash

# Copyright © VNG Realisatie 2019-2021
# Licensed under the EUPL

set -e

cd /opt/haven

echo -ne "[Haven] About to create cluster '$CLUSTER'.\n\nContinue [y/n] "; read CONTINUE; echo; [[ "y" != $CONTINUE ]] && (echo "ABORTED"; exit 1)

# Init.
source $STATEPATH/cluster.env

if [[ "openstack" != $CLOUD ]]; then
    echo "[Haven] Cloud '${CLOUD}' is not supported by Haven at this moment. ABORTING."

    exit 1
fi

if [[ $(grep -L "%BASTION_IP%" $STATEPATH/ssh_config) ]]; then
    echo "[Haven] It looks like you are recreating your cluster. Please exit Haven, cleanup your state repository (rm ssh_config) and try again."

    exit 1
fi

# Create cluster.
kops create cluster \
  --cloud ${CLOUD} \
  --name "${CLUSTER}.k8s.local" \
  --state "${KOPS_STATE_STORE}" \
  --master-zones "${ZONES}" \
  --zones "${ZONES}" \
  --network-cidr "${CIDR}" \
  --image "${IMAGE}" \
  --master-count=${MASTER_COUNT} \
  --node-count=${NODE_COUNT} \
  --master-size "${MASTER_SIZE}" \
  --node-size "${NODE_SIZE}" \
  --etcd-storage-type "${ETCD_STORAGE}" \
  --topology "private" \
  --bastion \
  --ssh-public-key "${STATEPATH}/ssh_id_rsa.pub" \
  --ssh-access="${BASTION_ALLOWED_CIDR}" \
  --api-loadbalancer-type "public" \
  --admin-access="${API_ALLOWED_CIDR}" \
  --networking "calico" \
  --os-dns-servers="1.1.1.1,1.0.0.1" \
  --os-ext-net="${EXTNET}" \
  --os-octavia=${USE_OCTAVIA}

# Harden config.
echo "[Haven] Hardening config."

kops get cluster -o yaml | \
sed "s/  cloudConfig\:/  kubeAPIServer\:\n    disableBasicAuth\: true\n  cloudConfig\:/" | \
kops replace -f -

# External CCM.
echo "[Haven] Enable external Cloud Controller Manager."

kops get cluster -o yaml | \
sed "s/spec\:/spec\:\n  cloudControllerManager\:\n    image\: docker.io\/k8scloudprovider\/openstack-cloud-controller-manager\:v${OPENSTACK_CLOUD_CONTROLLER_MANAGER_VERSION}/" | \
kops replace -f -

# Manage security groups.
echo "[Haven] Manage API LB security groups."

kops get cluster -o yaml | \
sed "s/loadbalancer\:/loadbalancer\:\n        manageSecurityGroups\: true/" | \
kops replace -f -

# Support metrics-server.
echo "[Haven] Support metrics-server."

KOPS_FEATURE_FLAGS=SpecOverrideFlag kops set cluster spec.kubelet.authenticationTokenWebhook=true
KOPS_FEATURE_FLAGS=SpecOverrideFlag kops set cluster spec.kubelet.authorizationMode=Webhook

# CNCF compliancy requirement. See: https://github.com/kubernetes/kops/issues/8129#issuecomment-605976292.
KOPS_FEATURE_FLAGS=SpecOverrideFlag kops set cluster cluster.spec.nodePortAccess=10.0.0.0/16

# Configure instance groups.
IGS=$(kops get ig -o json | jq '.[] | .metadata.name')

for IG in $IGS; do
    IG=$(echo $IG | tr -d '"')

    kops get ig $IG -o yaml > /tmp/ig.yaml

    # OpenStack: set volume size for nodes and masters (requires kops 1.16+).
    if [[ "openstack" == $CLOUD ]]; then
        VOLSIZE=-1

        if $(echo $IG | grep -q "bastions"); then
            VOLSIZE=$BASTION_DISK
        elif $(echo $IG | grep -q "master"); then
            VOLSIZE=$MASTER_DISK
        elif $(echo $IG | grep -q "nodes"); then
            VOLSIZE=$NODE_DISK
        fi

        if [[ -1 != $VOLSIZE ]]; then
            echo "[Haven] Instance Group '$IG': set volume size to ${VOLSIZE}gb."

            sed -i "s/name\: $IG/name\: $IG\n  annotations\:\n    openstack.kops.io\/osVolumeBoot\: enabled\n    openstack.kops.io\/osVolumeSize\: \"$VOLSIZE\"/" /tmp/ig.yaml
        fi
    fi

    if [[ "bastions" != $IG ]]; then
        # Extend cloud-init: Switch from iptables-nft to iptables-legacy as NFT is not supported yet.
        # https://kops.sigs.k8s.io/operations/images/#debian-10-buster
        echo "[Haven] Instance Group '$IG': cloud-init: switch to iptables-legacy."

        sed -i "s/spec\:/spec\:\n  additionalUserData\:\n  - name\: busterfix.sh\n    type\: text\/x-shellscript\n    content\: |\n      #\!\/bin\/sh\n      update-alternatives --set iptables \/usr\/sbin\/iptables-legacy\n      update-alternatives --set ip6tables \/usr\/sbin\/ip6tables-legacy\n      update-alternatives --set arptables \/usr\/sbin\/arptables-legacy\n      update-alternatives --set ebtables \/usr\/sbin\/ebtables-legacy/" /tmp/ig.yaml

        # Extend cloud-init: Disable swap (required by kubelet). First extension so this one adds the additionalUserData block.
        echo "[Haven] Instance Group '$IG': cloud-init: disable swap."

        sed -i "s/  additionalUserData\:/  additionalUserData\:\n  - name\: noswap.sh\n    type\: text\/x-shellscript\n    content\: |\n      #\!\/bin\/sh\n      swapoff -a\n      sed -i \"\/swap\/d\" \/etc\/fstab/" /tmp/ig.yaml

        # Extend cloud-init: Enable /var/log/journal preparing for log harvesters like fluentd and Loki's promtail.
        echo "[Haven] Instance Group '$IG': cloud-init: enable /var/log/journal preparing for log harvesters."

        sed -i "s/  additionalUserData\:/  additionalUserData\:\n  - name\: journal.sh\n    type\: text\/x-shellscript\n    content\: |\n      #\!\/bin\/sh\n      mkdir -p \/var\/log\/journal\n      systemd-tmpfiles --create --prefix \/var\/log\/journal/" /tmp/ig.yaml
    fi

    # Persist.
    echo "---" >> /tmp/igs.yaml
    cat /tmp/ig.yaml >> /tmp/igs.yaml
    rm -f /tmp/ig.yaml
done

kops replace -f /tmp/igs.yaml

rm -f /tmp/igs.yaml

# Allow for user customization beforehand.
read -rep $'\n[Haven] Would you like to make custom changes to the cluster kops template? [y/n]: ' CONTINUE
echo

if [[ "y" == "$CONTINUE" ]]; then
    echo

    kops edit cluster

    echo
fi

kops update cluster --yes

# Export kubeconfig. Requires quarterly refresh.
kops export kubecfg --admin=2610h

# Encrypt kube config.
cp /tmp/kubectl.conf $STATEPATH

echo "[Haven] Found unencrypted kubectl.conf."

ansible-vault encrypt $STATEPATH/kubectl.conf --vault-password-file $KEYFILE

# Configure SSH and bastion access.
if [[ "openstack" == $CLOUD ]]; then
    echo -e "\n[Haven] Enabling bastion ssh config\n"

    BASTION_IP=$(openstack server list -f json | jq -c '.[]' | grep -E "bastions-(\d)+-${CLUSTER}" | jq .Networks | tr -d '"' | awk -F ', ' '{print $2}')

    sed -i "s/%BASTION_IP%/${BASTION_IP}/" ${STATEPATH}/ssh_config

    ssh-keyscan -H $BASTION_IP 2>/dev/null >> /root/.ssh/known_hosts || echo "[Haven] FATAL: ssh-keyscan failed to add bastion. Is your ip whitelisted? Fix manually and run create script again sourcing cluster.env and starting below ssh-keyscan."
fi

# Wait for kops to validate the cluster.
# XXX: If the cluster does not become healthy you may be hitting https://github.com/kubernetes/kops/issues/11323. Workaround: manually mkfs.ext4 /dev/vdc or vdd depending on which one is not yet mounted, or running udevadm trigger.
while ! kops validate cluster 2>&1 | grep "Your cluster ${CLUSTER}.k8s.local is ready" >/dev/null; do echo "[Haven] $(date) Waiting 1 minute for cluster to become ready (kops validate cluster), usually takes about 15 minutes"; sleep 60s; done

# Install metrics-server.
helm install metrics-server --namespace kube-system --version $METRICS_CHART_VERSION --set args={"--kubelet-insecure-tls=true, --kubelet-preferred-address-types=InternalIP"} stable/metrics-server

# Backup kops state.
kops get -o yaml > $STATEPATH/kops-${CLUSTER}-$(date -u '+%Y%m%d%H%I%S').yaml

# Done.
echo -e "\n[Haven] Created cluster '$CLUSTER'.\n"
