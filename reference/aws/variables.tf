variable "region" {
  description = "The region to deploy the cluster in"
  default = "eu-central-1"
}

variable "clustername" {
  description = "The name of the cluster"
  default = "haven"
}

variable "endpoint_private_access" {
  description = "Indicates whether or not the Amazon EKS private API server endpoint is enabled"
  default = false
}

variable "endpoint_public_access" {
  description = "Indicates whether or not the Amazon EKS public API server endpoint is enabled"
  default = true
}

variable "endpoint_public_access_cidrs" {
  description = "List of CIDR blocks which can access the Amazon EKS public API server endpoint."
  default = ["0.0.0.0/0"]
}

variable "availability_zones" {
  description = "The list of availability zones to create the VPC in"
  default = ["eu-central-1a", "eu-central-1b", "eu-central-1c"]
}

variable "vpc_cidr" {
  description = "The CIDR of the VPC"
  default = "10.254.0.0/16"
}

variable "private_subnets" {
  description = "A map of private subnets"
  default = ["10.254.0.0/20", "10.254.16.0/20", "10.254.32.0/20"]
}

variable "public_subnets" {
  description = "A map of public subnets"
  default = ["10.254.128.0/20", "10.254.144.0/20", "10.254.160.0/20"]
}

variable "instance_type" {
  description = "The type of the instances in the default node group"
  default = "t3.medium"
}

variable "scaling_config_desired_size" {
  description = "The desired number of nodes"
  default = 3
}
