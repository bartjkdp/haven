# Reference Implementation: Haven on Amazon (AWS)

## EKS
Haven can easily be deployed on Amazon using [EKS](https://aws.amazon.com/eks/).

## How it works
Follow the steps from the [online documentation](https://haven.commonground.nl/docs/aan-de-slag/aws) or have a look at the [terraform code](./).

When ready ensure your new cluster is Haven Compliant by using the [Haven Compliancy Checker](../../haven/cli/README.md).

## Addons
Please look into the [Haven CLI](../../haven/cli/README.md) for addons management.

## License
Copyright © VNG Realisatie 2019-2021
Licensed under the EUPL
