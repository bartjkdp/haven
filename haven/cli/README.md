# Haven CLI

The Haven CLI can be used to validate an existing cluster on Haven Compliancy and to manage installation of addons.

## Usage
Try `haven --help`.

Global arguments:
- Enable logging to file with --log-file <path>. Logs may include debug level information not printed to screen.
- Specify the output format using --output <text|json>. By default output will is set to text. (Currently applies to the Compliancy Checker only)

In general when you output to JSON it's recommended to enable logging to file as well, especially with longer execution times needed by the Compliancy Checker. By tailing the logs you'll see progress during execution.

### Compliancy Checker
Haven Compliancy Checker runs against existing Kubernetes clusters, checking for Haven Compliancy by executing multiple checks.

See `haven check --help`.

- Opt in to checks requiring SSH access with --ssh-host <ip-address-of-any-master-node>. *SSH checks are required for Haven Compliancy*.
- Opt out of external CNCF checks with `haven check --cncf=false`. *CNCF checks are required for Haven Compliancy*.
- Opt in to external CIS benchmark checks with `haven check --cis`. CIS checks are optional and not required for Haven Compliancy.

*Haven Compliancy thus requires your final command to end up like this: `haven check --ssh-host 172.31.3.37`*.

Some examples:
- Every option made explicit: `haven --output text --log-file output.log check --ssh-host 10.0.0.42 --cncf --cis`.
- Output results in JSON format for automation purposes: `haven --output json check --ssh-host 192.168.10.12`.

### Addons
Community addons are an easy but optional way to deploy basics like logging on your Haven cluster, by using the Haven CLI.

See `haven addons --help`.

For example installing the monitoring addon can be done using: `haven addons install monitoring`.
The Haven CLI will interactively guide the user through required configuration parameters like providing a hostname.

Addons are not required for Haven Compliancy.

### Dashboard
The Haven dashboard allows easy installation of Helm charts through a web interface. Deployments are managed by [Flux](https://toolkit.fluxcd.io/).

Start the dashboard with `haven dashboard`.

Haven Dashboard is part of the Community addons and is not required for Haven Compliancy.

## License
Copyright © VNG Realisatie 2019-2021
Licensed under the EUPL
