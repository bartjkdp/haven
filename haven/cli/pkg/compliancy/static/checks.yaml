# Copyright © VNG Realisatie 2019-2021
# Licensed under EUPL v1.2
#
# Checks.yaml is the single source of truth containing all the Haven Compliancy Checker input
# required to validate a cluster.
#
# NOTE: pkg/compliancy/checks.go: checkFuncMapping must be kept in sync.

compliancychecks:
  - name: clusteradmin
    label: "Self test: does HCC have cluster-admin"
    category: Fundamental
    rationale: In order for the Haven Compliancy Checker to function properly elevated privileges are required.
    sshrequired: false
  - name: multiaz
    label: "Multiple availability zones in use"
    category: Infrastructure
    rationale: Running a cluster on a single availability zone means a higher risk of downtime when that single zone runs into problems.
    sshrequired: false
  - name: hamasters
    label: "Running at least 3 master nodes"
    category: Infrastructure
    rationale: This ensures a highly available control plane.
    sshrequired: false
  - name: haworkers
    label: "Running at least 3 worker nodes"
    category: Infrastructure
    rationale: This enables running highly available workloads.
    sshrequired: false
  - name: nodehardening
    label: "Nodes have SELinux, Grsecurity, AppArmor or LKRG enabled"
    category: Infrastructure
    rationale: Security matters on every layer of a system and should an attacker break out of a deployment onto a node increased node security will help prevent further escalation.
    sshrequired: true
  - name: privatenetworking
    label: "Private networking topology"
    category: Infrastructure
    rationale: Not directly exposing masters or workers to the public internet can increase the security of the cluster.
    sshrequired: false
  - name: kubernetesversion
    label: "Kubernetes version is latest stable or max 2 minor versions behind"
    category: Cluster
    rationale: This allows cluster users to access new functionality quickly and encourages a well-implemented update mechanism.
    sshrequired: false
  - name: rbac
    label: "Role Based Access Control is enabled"
    category: Cluster
    rationale: Basic security option which is enabled by default in order to control who can do what on a cluster.
    sshrequired: false
  - name: basicauth
    label: "Basic auth is disabled"
    category: Cluster
    rationale: Basic authentication is hard to maintain. We encourage to use OpenID Connect for user authentication.
    sshrequired: true
  - name: rwxvolumes
    label: "ReadWriteMany persistent volumes support"
    category: Cluster
    rationale: This ensures that storage can be created which can be used by highly available deployments.
    sshrequired: false
  - name: loadbalancers
    label: "LoadBalancer service type support"
    category: Cluster
    rationale: This ensures that Layer 4 loadbalancers can automatically be created from the Kubernetes API.
    sshrequired: false
  - name: cncf
    label: "CNCF Kubernetes Conformance"
    category: External
    rationale: Cloud Native Computing Foundation's Kubernetes checks ensure the Kubernetes cluster adheres to the standard Kubernetes API's.
    sshrequired: false
  - name: autocerts
    label: "Automated HTTPS certificate provisioning"
    category: Deployment
    rationale: This makes it easy for engineers to expose an Ingress with a valid SSL certificate which automatically renews.
    sshrequired: false
  - name: logs
    label: "Log aggregation is running"
    category: Deployment
    rationale: In order to be in control of the workload on a cluster it's mandatory to aggregate all container logs.
    sshrequired: false
  - name: metrics
    label: "Metrics-server is running"
    category: Deployment
    rationale: In order to be in control of a cluster it's mandatory to have eyes an ears on the cluster resources.
    sshrequired: false
suggestedchecks:
  - name: dashboard
    label: "Haven Dashboard is provisioned"
    category: Deployment
    rationale: The community dashboard from Haven allows for an easy way to manage deployments.
    sshrequired: false
  - name: cis
    label: "CIS Kubernetes Security Benchmark"
    category: External
    rationale: Center for Internet Security's Kubernetes benchmark assists in following the best practices of securing a cluster.
    sshrequired: false
