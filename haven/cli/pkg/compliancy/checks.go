// Copyright © VNG Realisatie 2019-2021
// Licensed under EUPL v1.2

package compliancy

// checkFuncMapping maps checks.yaml id's to golang methods.
var checkFuncMapping = map[string]func(*Config) (Result, error){
	"clusteradmin":      fundamentalSelfTest,
	"multiaz":           infraMultiAZ,
	"hamasters":         infraMultiMaster,
	"haworkers":         infraMultiWorker,
	"nodehardening":     infraSecuredNodes,
	"privatenetworking": infraPrivateTopology,
	"kubernetesversion": clusterVersion,
	"rbac":              clusterRBAC,
	"basicauth":         clusterBasicAuth,
	"rwxvolumes":        clusterVolumes,
	"loadbalancers":     clusterLoadBalancerService,
	"cncf":              externalCNCF,
	"autocerts":         deploymentHttpsCerts,
	"logs":              deploymentLogAggregation,
	"metrics":           deploymentMetricsServer,
	"dashboard":         suggestionDeploymentHavenDashboard,
	"cis":               suggestionExternalCIS,
}

// Checks contain all compliancy and suggested checks sourced from checks.yaml.
type Checks struct {
	CompliancyChecks []Check
	SuggestedChecks  []Check
}

// Check represents a single Compliancy or Suggested check.
type Check struct {
	Name        string
	Label       string
	Category    Category
	Rationale   string
	SSHRequired bool
	Result      Result `json:",omitempty"`
}

func (c Check) Exec(config *Config) (Result, error) {
	return checkFuncMapping[c.Name](config)
}
