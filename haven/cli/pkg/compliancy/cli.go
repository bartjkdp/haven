// Copyright © VNG Realisatie 2019-2021
// Licensed under EUPL v1.2

package compliancy

import (
	"embed"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os/exec"
	"time"

	cli "github.com/jawher/mow.cli"
	"sigs.k8s.io/yaml"

	"gitlab.com/commonground/haven/haven/haven/cli/pkg/connect"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/logging"
)

var (
	kubeLatest = "undefined"
)

// CLIConfig renders the command line function
var CLIConfig = func(config *cli.Cmd) {
	rationale := config.BoolOpt("rationale", false, "[Compliancy] Displays the reasoning behind all Compliancy Checks (does not run the Checker).")
	sshHost := config.StringOpt("ssh-host", "", "[Compliancy] SSH host name of a master node. Required, results in skipped tests when omitted.")
	runCNCFChecks := config.BoolOpt("cncf", true, "[Compliancy] Run external CNCF checks. Required, results in skipped test when set to false.")
	runCISChecks := config.BoolOpt("cis", false, "[Suggestion] Run external CIS checks by specifying log output file path. Optional.")

	config.Action = func() {
		if err := initChecker(); err != nil {
			fmt.Printf("Fatal error initializing Haven Compliancy Checker: %s.\n", err.Error())

			return
		}

		if *rationale {
			cmdRationale()

			return
		}

		cmdCheck(sshHost, runCNCFChecks, runCISChecks)
	}
}

var (
	Version string

	checks    Checks
	output    CompliancyOutput
	rationale RationaleOutput

	sshConfigured  bool = false
	cncfConfigured bool = false

	//go:embed static/checks.yaml
	embedFiles embed.FS
)

// initChecker sources checks.yaml and preps output.
func initChecker() error {
	rationale.Version = fmt.Sprintf("Haven %s", Version)
	output.Version = fmt.Sprintf("Haven %s", Version)
	output.StartTS = time.Now()

	f, err := embedFiles.Open("static/checks.yaml")
	if err != nil {
		return err
	}

	defer f.Close()

	b, err := ioutil.ReadAll(f)
	if err != nil {
		return err
	}

	err = yaml.Unmarshal(b, &checks)
	if err != nil {
		return err
	}

	return nil
}

// cmdCheck validates requirements, runs the in scope checks and outputs
// results.
func cmdCheck(sshHost *string, runCNCFChecks *bool, runCISChecks *bool) {
	logging.Debug("HAVEN COMPLIANCY CHECKER - STARTED\n")
	defer logging.Debug("HAVEN COMPLIANCY CHECKER - FINISHED\n\n\n")

	kubeConfig, err := connect.K8s()
	if err != nil {
		logging.Fatal("Could not connect to Kubernetes: %s\n", err.Error())
		return
	}

	if "" != *sshHost {
		err := connect.SSH(sshHost)
		if err != nil {
			logging.Fatal("SSH: Could not connect to host: %s\n", err.Error())
			return
		}

		sshConfigured = true
		logging.Info("SSH: Configured host '%s'. Enabled checks requiring SSH access.\n", *sshHost)
	}

	if !sshConfigured {
		logging.Warning("SSH: Not configured! Skipping compliancy checks requiring SSH access.\n")
	}

	output.Config.SSH = sshConfigured

	if *runCNCFChecks {
		if path, err := exec.LookPath("sonobuoy"); err == nil {
			cncfConfigured = true

			logging.Info("CNCF: Sonobuoy binary found at '%s'. Enabled external CNCF compliancy check.\n", path)
		} else {
			logging.Warning("CNCF: Sonobuoy binary not found! Skipping external CNCF compliancy check.\n")
		}
	} else {
		logging.Warning("CNCF: Opted out! Skipping external CNCF compliancy check.\n")
	}

	output.Config.CNCF = cncfConfigured

	if *runCISChecks {
		logging.Info("CIS: Opted in. Enabled external CIS suggested check.\n")
	} else {
		logging.Info("CIS: Not opted in. Skipping external CIS suggested check.\n")
	}

	output.Config.CIS = *runCISChecks

	logging.Info("Running checks...")

	checker, err := NewChecker(kubeConfig, kubeLatest, *sshHost, cncfConfigured, *runCISChecks)
	if err != nil {
		logging.Fatal("Could not start checker: %s\n", err.Error())
		return
	}

	err = checker.Run()
	if err != nil {
		logging.Error("Checker encountered an error: %s\n", err.Error())
		return
	}

	output.CompliancyChecks.Results = checks.CompliancyChecks
	output.SuggestedChecks.Results = checks.SuggestedChecks
	output.StopTS = time.Now()

	checker.PrintResults(checks.CompliancyChecks, true)
	checker.PrintResults(checks.SuggestedChecks, false)

	if *logging.OutputFormat == "json" {
		if err := printJson(output); err != nil {
			logging.Error("Could not print JSON output: %s\n", err.Error())
			return
		}
	}
}

// cmdRationale outputs a table based on checks.yaml with all Compliancy and Suggested checks
// with the reasoning behind these checks.
func cmdRationale() {
	logging.Info("The Haven Compliancy Checker contains %d required Compliancy Checks to validate a cluster.", len(checks.CompliancyChecks))
	logging.Info("Compliancy Checks are restricted to essentials allowing for maximum flexibility while keeping the Haven promise of interoperability.")
	logging.Info("Besides the required checks there are %d Suggested Checks which can assist further enhancement of Haven environments.\n\n\n", len(checks.SuggestedChecks))

	logging.Info("** Compliancy Checks **\n\n")
	for _, c := range checks.CompliancyChecks {
		logging.Info(fmt.Sprintf("%s\n%s\n\n", c.Label, c.Rationale))
	}

	logging.Info("** Suggested Checks **\n\n")
	for _, s := range checks.SuggestedChecks {
		logging.Info(fmt.Sprintf("%s\n%s\n\n", s.Label, s.Rationale))
	}

	if *logging.OutputFormat == "json" {
		rationale.CompliancyChecks = checks.CompliancyChecks
		rationale.SuggestedChecks = checks.SuggestedChecks

		if err := printJson(rationale); err != nil {
			logging.Error("Print as JSON: %s", err)
		}
	}
}

// printJson builds a results json object and prints it to screen.
func printJson(output interface{}) error {
	b, err := json.Marshal(output)
	if err != nil {
		return err
	}

	fmt.Printf("%s\n", string(b))

	return nil
}
