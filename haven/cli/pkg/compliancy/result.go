// Copyright © VNG Realisatie 2019-2021
// Licensed under EUPL v1.2

package compliancy

// Result represents a check result
type Result string

func (r Result) String() string {
	return string(r)
}

const (
	// ResultUnknown is returned when a test could not be performed
	ResultUnknown Result = "UNKNOWN"
	// ResultSkipped is returned when a test is skipped
	ResultSkipped Result = "SKIPPED"
	// ResultNo is returned when a test fails
	ResultNo Result = "NO"
	// ResultYes is returned when a test succeeds
	ResultYes Result = "YES"
)
