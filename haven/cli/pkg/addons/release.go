// Copyright © VNG Realisatie 2019-2021
// Licensed under EUPL v1.2

package addons

import (
	"io/ioutil"
	"strings"

	"sigs.k8s.io/yaml"
)

// Release refers to a Helm chart on a specific repository
type Release struct {
	Name              string
	Chart             Chart
	DefaultValuesFile string
}

// GetValues opens the DefaultValuesFile, performs a search-and-replace for customValues and unmarshals the contents
func (r *Release) GetValues(customValues map[string]interface{}) (map[string]interface{}, error) {
	f, err := embedFiles.Open(r.DefaultValuesFile)
	if err != nil {
		return nil, err
	}

	defer f.Close()

	b, err := ioutil.ReadAll(f)
	if err != nil {
		return nil, err
	}

	s := string(b)

	for key, value := range customValues {
		newValue, ok := value.(string)
		if !ok {
			continue
		}

		s = strings.ReplaceAll(s, key, newValue)
	}

	values := make(map[string]interface{})

	err = yaml.Unmarshal([]byte(s), &values)
	if err != nil {
		return nil, err
	}

	return values, nil
}
