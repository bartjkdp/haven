// Copyright © VNG Realisatie 2019-2021
// Licensed under EUPL v1.2

package addons

import (
	cli "github.com/jawher/mow.cli"

	"gitlab.com/commonground/haven/haven/haven/cli/pkg/helm"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/logging"
)

func cmdUninstall(cmd *cli.Cmd) {
	name := cmd.StringArg("NAME", "", "The addon to install")

	cmd.Action = func() {
		addon, err := Get(*name)
		if err != nil {
			logging.Error("Error while getting addon: %s\n", err)
			return
		}

		helmClient, err := helm.NewClient(addon.Namespace)
		if err != nil {
			logging.Error("Error constructing client: %s\n", err)
			return
		}

		for _, release := range addon.Releases {
			logging.Info("Uninstalling %s\n", release.Name)

			_, err := helmClient.Uninstall(release.Name)
			if err != nil {
				logging.Error("Error while uninstalling: %s\n", err)
			}
		}

		logging.Info("Uninstalled addon %s\n", *name)
	}
}
