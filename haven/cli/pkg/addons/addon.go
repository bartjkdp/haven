// Copyright © VNG Realisatie 2019-2021
// Licensed under EUPL v1.2

package addons

import (
	"github.com/AlecAivazis/survey/v2"
	"golang.org/x/crypto/bcrypt"

	"gitlab.com/commonground/haven/haven/haven/cli/pkg/rand"
)

// Addon is installable on a Haven cluster
type Addon struct {
	Name            string
	Namespace       string
	Description     string
	Questions       []Question
	GeneratedValues []GeneratedValue
	Releases        []Release
	Resources       []string
}

// GetSurveyQuestions returns questions in the form that survey expects
func (a *Addon) GetSurveyQuestions() []*survey.Question {
	surveyQuestions := []*survey.Question{}

	for _, question := range a.Questions {
		var defaultValue string
		if question.Default.RandString > 0 {
			defaultValue = rand.String(question.Default.RandString)
		}
		if question.Default.String != "" {
			defaultValue = question.Default.String
		}

		var transform func(s interface{}) interface{}
		if question.Transform.Password == "bcrypt" {
			transform = func(s interface{}) interface{} {
				inputString, ok := s.(string)
				if !ok {
					return ""
				}

				b, err := bcrypt.GenerateFromPassword([]byte(inputString), bcrypt.DefaultCost)
				if err != nil {
					return ""
				}

				return string(b)
			}
		}

		surveyQuestions = append(surveyQuestions, &survey.Question{
			Name: question.Name,
			Prompt: &survey.Input{
				Message: question.Message,
				Help:    question.Help,
				Default: defaultValue,
			},
			Transform: transform,
		})
	}

	return surveyQuestions
}
