// Copyright © VNG Realisatie 2019-2021
// Licensed under EUPL v1.2

package addons

import (
	"embed"
	"errors"
	"io/ioutil"

	"sigs.k8s.io/yaml"
)

// Config of the addons
type Config struct {
	Addons []Addon
}

// Question is an interactive question
type Question struct {
	Name      string
	Message   string
	Help      string
	Default   QuestionDefault
	Transform QuestionTransform
}

// QuestionDefault defines possible default values for Question
type QuestionDefault struct {
	String     string
	RandString int
}

// QuestionTransform defines possible transforms on Question answers
type QuestionTransform struct {
	Password string
}

// GeneratedValue is generated on installation
type GeneratedValue struct {
	Name       string
	RandString int
}

// Chart refers to a Helm chart
type Chart struct {
	Name       string
	Repository Repository
}

// Repository refers to a Helm repository
type Repository struct {
	Name string
	URL  string
}

//go:embed static/**
var embedFiles embed.FS

// List all addons
func List() ([]Addon, error) {
	f, err := embedFiles.Open("static/addons.yaml")
	if err != nil {
		return nil, err
	}

	defer f.Close()

	b, err := ioutil.ReadAll(f)
	if err != nil {
		return nil, err
	}

	var config Config

	err = yaml.Unmarshal(b, &config)
	if err != nil {
		return nil, err
	}

	return config.Addons, nil
}

// Get a specific addon by name
func Get(name string) (*Addon, error) {
	addons, err := List()
	if err != nil {
		return nil, err
	}

	for _, addon := range addons {
		if addon.Name == name {
			return &addon, nil
		}
	}

	return nil, errors.New("could not find addon")
}
