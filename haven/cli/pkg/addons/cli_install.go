// Copyright © VNG Realisatie 2019-2021
// Licensed under EUPL v1.2

package addons

import (
	"fmt"

	"github.com/AlecAivazis/survey/v2"
	cli "github.com/jawher/mow.cli"

	"gitlab.com/commonground/haven/haven/haven/cli/pkg/helm"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/logging"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/rand"
)

func cmdInstall(cmd *cli.Cmd) {
	name := cmd.StringArg("NAME", "", "The addon to install")

	cmd.Action = func() {
		addon, err := Get(*name)
		if err != nil {
			logging.Error("Error while getting addon: %s\n", err)
			return
		}

		helmClient, err := helm.NewClient(addon.Namespace)
		if err != nil {
			logging.Error("Error constructing client: %s\n", err)
			return
		}

		customValues := map[string]interface{}{}
		err = survey.Ask(addon.GetSurveyQuestions(), &customValues)
		if err != nil {
			logging.Error("Error processing answers: %s\n", err)
			return
		}

		for _, generatedValue := range addon.GeneratedValues {
			customValues[generatedValue.Name] = rand.String(generatedValue.RandString)
		}

		for _, release := range addon.Releases {
			logging.Info("Adding repository %s\n", release.Chart.Repository.Name)

			_, err = helmClient.AddRepository(release.Chart.Repository.Name, release.Chart.Repository.URL)
			if err != nil {
				logging.Error("Error adding repository: %s\n", err)
			}

			logging.Info("Installing %s\n", release.Name)

			values, err := release.GetValues(customValues)
			if err != nil {
				logging.Error("Error getting default values: %s\n", err)
				return
			}

			_, err = helmClient.Install(
				release.Name,
				fmt.Sprintf("%s/%s", release.Chart.Repository.Name, release.Chart.Name),
				values,
			)

			if err != nil {
				logging.Error("Error installing release: %s\n", err)
				return
			}
		}

		logging.Info("Installed addon %s in namespace %s\n", addon.Name, addon.Namespace)
	}
}
