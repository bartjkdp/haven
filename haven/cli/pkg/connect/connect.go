// Copyright © VNG Realisatie 2019-2021
// Licensed under EUPL v1.2

package connect

import (
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"

	"gitlab.com/commonground/haven/haven/haven/cli/pkg/logging"
)

// K8s attempts to connect to a Kubernetes cluster via kube config or
// in cluster.
func K8s() (*rest.Config, error) {
	var cfg *rest.Config
	var err error
	var path string

	path = os.Getenv("KUBECONFIG")
	if "" == path {
		var home = os.Getenv("HOME")
		if "" == home {
			home = os.Getenv("USERPROFILE")
		}

		path = filepath.Join(home, ".kube", "config")
	}

	if _, err = os.Stat(path); err == nil {
		cfg, err = clientcmd.BuildConfigFromFlags("", path)
		if err != nil {
			logging.Error("Error while parsing KUBECONFIG: %s.\n", err)
			return nil, err
		}

		logging.Info("Kubernetes connection using KUBECONFIG: %s.\n", path)
	}

	if nil == cfg {
		cfg, err = rest.InClusterConfig()
		if err != nil {
			return nil, err
		}

		logging.Info("Kubernetes connection using In Cluster config.\n")
	}

	return cfg, nil
}

// SSH attempts to connect to the given node and assert this works.
func SSH(sshHost *string) error {
	c := strings.Split(fmt.Sprintf("ssh %s ''%s''", *sshHost, "uname -a"), " ")
	cmd := exec.Command(c[0], c[1:]...)

	cfg := `
ForwardAgent no
ServerAliveInterval 120

Host bastion
  User core
  IdentityFile ~/tmp/ssh_id_rsa
  Hostname 11.22.33.44

Host 10.0.101.*
  User core
  IdentityFile ~/tmp/ssh_id_rsa
  ProxyJump bastion
				`

	if err := cmd.Run(); err != nil {
		return fmt.Errorf("SSH: configured host '%s', but connection failed: '%s'\n\nTry to configure ~/.ssh/config like this:\n%s\n\n", *sshHost, err.Error(), cfg)
	}

	return nil
}
