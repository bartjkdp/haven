// Copyright © VNG Realisatie 2019-2021
// Licensed under EUPL v1.2

package helm

import (
	"io/ioutil"
	"os"
	"path/filepath"

	"helm.sh/helm/v3/pkg/getter"
	"helm.sh/helm/v3/pkg/repo"
	"sigs.k8s.io/yaml"
)

// AddRepository adds a new Helm repository
func (h *Client) AddRepository(name string, url string) (*repo.ChartRepository, error) {
	err := os.MkdirAll(filepath.Dir(settings.RepositoryConfig), os.ModePerm)
	if err != nil && !os.IsExist(err) {
		return nil, err
	}

	b, err := ioutil.ReadFile(settings.RepositoryConfig)
	if err != nil && !os.IsNotExist(err) {
		return nil, err
	}

	var f repo.File
	if err := yaml.Unmarshal(b, &f); err != nil {
		return nil, err
	}

	c := repo.Entry{
		Name: name,
		URL:  url,
	}

	r, err := repo.NewChartRepository(&c, getter.All(settings))
	if err != nil {
		return nil, err
	}

	if _, err := r.DownloadIndexFile(); err != nil {
		return nil, err
	}

	f.Update(&c)

	if err := f.WriteFile(settings.RepositoryConfig, 0644); err != nil {
		return nil, err
	}

	return r, nil
}
