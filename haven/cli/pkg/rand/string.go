// Copyright © VNG Realisatie 2019-2021
// Licensed under EUPL v1.2

package rand

import (
	crypto_rand "crypto/rand"
	"encoding/binary"
	math_rand "math/rand"
)

const charset = "abcdefghijklmnopqrstuvwxyz" +
	"ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

// StringWithCharset generates a random string from a specific charset
func StringWithCharset(length int, charset string) string {
	var seed [8]byte
	_, err := crypto_rand.Read(seed[:])
	if err != nil {
		panic("cannot seed math/rand package with cryptographically secure random number generator")
	}
	math_rand.Seed(int64(binary.LittleEndian.Uint64(seed[:])))

	b := make([]byte, length)
	for i := range b {
		b[i] = charset[math_rand.Intn(len(charset))]
	}
	return string(b)
}

// String generates a random string with a specified length
func String(length int) string {
	return StringWithCharset(length, charset)
}
