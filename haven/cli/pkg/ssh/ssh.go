// Copyright © VNG Realisatie 2019-2021
// Licensed under EUPL v1.2

package ssh

import (
	"fmt"
	"os/exec"
)

// Run a specific command on an external host through SSH and
// return the combined stdout and stderr output or an error.
// Uses shell calls instead of a go ssh client, because of the complexity
// certain ssh configs may bring (ProxyJump(s), IdentityFile(s), etc).
func Run(host string, command string) (string, error) {
	cmd := exec.Command("ssh", host, "sh", "-c", fmt.Sprintf("\"%s\"", command))

	out, err := cmd.CombinedOutput()

	if err != nil {
		return "", err
	}

	return string(out), nil
}
