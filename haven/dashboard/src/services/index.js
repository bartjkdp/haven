// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
import { HelmReleasesService } from './helmReleases'
import { HelmRepositoriesService } from './helmRepositories'
import { NamespacesService } from './namespaces'

const services = {
  helmReleases: new HelmReleasesService(),
  helmRepositories: new HelmRepositoriesService(),
  namespaces: new NamespacesService(),
}

export default services
