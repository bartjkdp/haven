// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
export class HelmRepositoriesService {
  async list() {
    const result = await fetch(
      '/apis/source.toolkit.fluxcd.io/v1beta1/helmrepositories/',
    )
    if (!result.ok) {
      throw new Error('error occured while getting repositories list')
    }

    return result.json()
  }

  async get(namespace, name) {
    const result = await fetch(
      `/apis/source.toolkit.fluxcd.io/v1beta1/namespaces/${namespace}/helmrepositories/${name}`,
    )
    if (!result.ok) {
      throw new Error('error occured while getting repository')
    }

    return result.json()
  }

  async create(namespace, repository) {
    const result = await fetch(
      `/apis/source.toolkit.fluxcd.io/v1beta1/namespaces/${namespace}/helmrepositories`,
      {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(repository),
      },
    )

    if (!result.ok) {
      throw new Error('error while creating repository')
    }

    return result.json()
  }

  async update(namespace, name, repository) {
    const result = await fetch(
      `/apis/source.toolkit.fluxcd.io/v1beta1/namespaces/${namespace}/helmrepositories/${name}`,
      {
        method: 'PUT',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(repository),
      },
    )

    if (!result.ok) {
      throw new Error('error while updating repository')
    }

    return result.json()
  }

  async delete(namespace, name) {
    const result = await fetch(
      `/apis/source.toolkit.fluxcd.io/v1beta1/namespaces/${namespace}/helmrepositories/${name}`,
      { method: 'DELETE' },
    )
    if (!result.ok) {
      throw new Error('error while deleting repository')
    }

    return result.json()
  }
}
