// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
import React, { useState } from 'react'
import { string } from 'prop-types'
import { useParams, useHistory } from 'react-router-dom'
import { Alert, Drawer } from '@commonground/design-system'
import { useTranslation } from 'react-i18next'
import usePromise from '../../hooks/use-promise'
import LoadingMessage from '../../components/LoadingMessage'
import services from '../../services'
import {
  StyledActionsBar,
  StyledRemoveButton,
  StyledSpecList,
} from './index.styles'

const NamespaceDetailPage = ({ parentUrl }) => {
  const { t } = useTranslation()
  const history = useHistory()

  const { name } = useParams()

  const [isRemoved, setIsRemoved] = useState(false)
  const { isReady, error, result } = usePromise(services.namespaces.get, name)

  const handleRemove = () => {
    if (window.confirm(t('Do you want to remove the repository?'))) {
      services.namespaces.delete(name)
      setIsRemoved(true)
    }
  }

  return (
    <Drawer noMask closeHandler={() => history.push(parentUrl)}>
      <Drawer.Header title={name} closeButtonLabel={t('Close')} />
      <Drawer.Content>
        {!isReady || (!error && !result) ? (
          <LoadingMessage />
        ) : error ? (
          <Alert variant="error">
            {t('Failed to load the namespace.', { name })}
          </Alert>
        ) : isRemoved ? (
          <Alert variant="success">
            {t('The namespace has been removed.')}
          </Alert>
        ) : result ? (
          <>
            <StyledActionsBar>
              <StyledRemoveButton onClick={handleRemove} />
            </StyledActionsBar>

            <StyledSpecList alignValuesRight>
              <StyledSpecList.Item
                title={t('Name')}
                value={result.metadata.name}
              />
            </StyledSpecList>
          </>
        ) : null}
      </Drawer.Content>
    </Drawer>
  )
}

NamespaceDetailPage.propTypes = {
  parentUrl: string,
}

export default NamespaceDetailPage
