// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
import React from 'react'
import { string, object } from 'prop-types'
import { Link, Route } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import { Alert, Button, Table } from '@commonground/design-system'
import PageTemplate from '../../components/PageTemplate'
import EmptyContentMessage from '../../components/EmptyContentMessage'
import LoadingMessage from '../../components/LoadingMessage'
import RepositoryDetailPage from '../../pages/RepositoryDetailPage'
import usePromise from '../../hooks/use-promise'
import services from '../../services'
import Status from './Status'
import {
  StyledActionsBar,
  StyledIconPlus,
  StyledCount,
  StyledAmount,
} from './index.styles'

const RepositoryRow = ({ metadata, status }) => (
  <Table.Tr
    to={`/repositories/${metadata.namespace}/${metadata.name}`}
    name={metadata.name}
  >
    <Table.Td>{metadata.name}</Table.Td>
    <Table.Td>{metadata.namespace}</Table.Td>
    <Table.Td>{status ? <Status status={status} /> : null}</Table.Td>
  </Table.Tr>
)

RepositoryRow.propTypes = {
  metadata: object,
  status: string,
}

const ListRepositoriesPage = () => {
  const { t } = useTranslation()
  const { isReady, error, result, reload } = usePromise(
    services.helmRepositories.list,
  )

  return (
    <PageTemplate>
      <PageTemplate.Header title={t('Repositories')} />

      <StyledActionsBar>
        <StyledCount>
          <StyledAmount>{result ? result.items.length : 0}</StyledAmount>
          <small>{t('Repositories')}</small>
        </StyledCount>
        <Button
          as={Link}
          to="/repositories/add"
          aria-label={t('Add repository')}
        >
          <StyledIconPlus />
          {t('Add repository')}
        </Button>
      </StyledActionsBar>

      {!isReady ? (
        <LoadingMessage />
      ) : error ? (
        <Alert variant="error">{t('Failed to load repositories.')}</Alert>
      ) : result != null && result.items.length === 0 ? (
        <EmptyContentMessage>
          {t('There are no repositories yet.')}
        </EmptyContentMessage>
      ) : result ? (
        <Table withLinks role="grid">
          <thead>
            <Table.TrHead>
              <Table.Th>{t('Name')}</Table.Th>
              <Table.Th>{t('Namespace')}</Table.Th>
              <Table.Th>{t('Status')}</Table.Th>
            </Table.TrHead>
          </thead>
          <tbody>
            {result.items.map((namespace, i) => (
              <RepositoryRow key={i} {...namespace} />
            ))}
          </tbody>
        </Table>
      ) : null}
      <Route path="/repositories/:namespace/:name">
        <RepositoryDetailPage
          parentUrl="/repositories"
          refreshHandler={reload}
        />
      </Route>
    </PageTemplate>
  )
}

export default ListRepositoriesPage
