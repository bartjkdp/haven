// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
import styled from 'styled-components'
import RemoveButton from '../../components/RemoveButton'
import SpecList from '../../components/SpecList'

export const StyledActionsBar = styled.div`
  display: flex;
  align-items: flex-start;
  justify-content: space-between;
  padding-bottom: ${(p) => p.theme.tokens.spacing05};
`

export const StyledRemoveButton = styled(RemoveButton)`
  margin-left: auto;
`

export const StyledSpecList = styled(SpecList)`
  margin-bottom: ${(p) => p.theme.tokens.spacing05};
`
