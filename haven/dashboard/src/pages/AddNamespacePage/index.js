// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//

import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { Alert } from '@commonground/design-system'
import PageTemplate from '../../components/PageTemplate'
import NamespaceForm from '../../components/NamespaceForm'
import services from '../../services'

const AddNamespacePage = () => {
  const { t } = useTranslation()
  const [isProcessing, setIsProcessing] = useState(false)
  const [isAdded, setIsAdded] = useState(false)
  const [error, setError] = useState(null)

  const submitNamespace = async (namespace) => {
    setError(false)
    setIsProcessing(true)

    try {
      await services.namespaces.create(namespace)
      setIsAdded(true)
    } catch (e) {
      setError(true)
      console.error(e)
    }

    setIsProcessing(false)
  }

  return (
    <PageTemplate>
      <PageTemplate.HeaderWithBackNavigation
        backButtonTo="/namespaces"
        title={t('Add namespace')}
      />

      {error ? (
        <Alert title={t('Failed adding namespace')} variant="error">
          {error}
        </Alert>
      ) : null}

      {isAdded && !error ? (
        <Alert variant="success">{t('The namespace has been added.')}</Alert>
      ) : null}

      {!isAdded ? (
        <NamespaceForm
          submitButtonText="Add"
          onSubmitHandler={submitNamespace}
          disableForm={isProcessing}
        />
      ) : null}
    </PageTemplate>
  )
}

export default AddNamespacePage
