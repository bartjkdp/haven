// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
import React from 'react'
import { object } from 'prop-types'
import { Link, Route } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import { Alert, Table, Button } from '@commonground/design-system'
import PageTemplate from '../../components/PageTemplate'
import EmptyContentMessage from '../../components/EmptyContentMessage'
import LoadingMessage from '../../components/LoadingMessage'
import usePromise from '../../hooks/use-promise'
import services from '../../services'
import NamespaceDetailPage from '../../pages/NamespaceDetailPage'
import {
  StyledActionsBar,
  StyledCount,
  StyledAmount,
  StyledIconPlus,
} from './index.styles'

const NamespaceRow = ({ metadata }) => (
  <Table.Tr to={`/namespaces/${metadata.name}`} name={metadata.name}>
    <Table.Td>{metadata.name}</Table.Td>
  </Table.Tr>
)

NamespaceRow.propTypes = {
  metadata: object,
}

const ListNamespacesPage = () => {
  const { t } = useTranslation()
  const { isReady, error, result, reload } = usePromise(
    services.namespaces.list,
  )

  return (
    <PageTemplate>
      <PageTemplate.Header title={t('Namespaces')} />

      <StyledActionsBar>
        <StyledCount>
          <StyledAmount>{result ? result.items.length : 0}</StyledAmount>
          <small>{t('Namespaces')}</small>
        </StyledCount>

        <Button as={Link} to="/namespaces/add" aria-label={t('Add namespace')}>
          <StyledIconPlus />
          {t('Add namespace')}
        </Button>
      </StyledActionsBar>

      {!isReady ? (
        <LoadingMessage />
      ) : error ? (
        <Alert variant="error">{t('Failed to load namespaces.')}</Alert>
      ) : result != null && result.items.length === 0 ? (
        <EmptyContentMessage>
          {t('There are no namespaces yet.')}
        </EmptyContentMessage>
      ) : result ? (
        <Table withLinks role="grid">
          <thead>
            <Table.TrHead>
              <Table.Th>{t('Name')}</Table.Th>
            </Table.TrHead>
          </thead>
          <tbody>
            {result.items.map((namespace, i) => (
              <NamespaceRow key={i} {...namespace} />
            ))}
          </tbody>
        </Table>
      ) : null}
      <Route path="/namespaces/:name">
        <NamespaceDetailPage parentUrl="/namespaces" refreshHandler={reload} />
      </Route>
    </PageTemplate>
  )
}

export default ListNamespacesPage
