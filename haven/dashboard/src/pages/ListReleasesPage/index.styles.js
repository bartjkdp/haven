// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//

import styled from 'styled-components'
import { ReactComponent as IconPlus } from './plus.svg'

export const StyledActionsBar = styled.div`
  display: flex;
  align-items: flex-start;
  justify-content: space-between;
`

export const StyledIconPlus = styled(IconPlus)`
  margin-right: ${(p) => p.theme.tokens.spacing03};
`

export const StyledCount = styled.div`
  margin-bottom: ${(p) => p.theme.tokens.spacing07};
`

export const StyledAmount = styled.span`
  font-weight: ${(p) => p.theme.tokens.fontWeightBold};
  font-size: ${(p) => p.theme.tokens.fontSizeXXLarge};
  display: block;
`
