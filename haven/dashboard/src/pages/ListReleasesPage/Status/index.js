// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
import React from 'react'
import { string } from 'prop-types'
import { useTranslation } from 'react-i18next'

import { ReactComponent as IconStatusUp } from './status-up.svg'
import { ReactComponent as IconStatusDown } from './status-down.svg'
import { StyledWrapper } from './index.styles'

const Status = ({ status }) => {
  const { t } = useTranslation()

  return (
    <StyledWrapper>
      {
        {
          True: <IconStatusUp title={t('Deployed')} />,
          False: <IconStatusDown title={t('Not deployed')} />,
        }[status.conditions[0].status]
      }
    </StyledWrapper>
  )
}

Status.propTypes = {
  status: string,
}

export default Status
