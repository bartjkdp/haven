# Haven

Copyright © VNG Realisatie 2019-2021<br />
Licensed under EUPL v1.2

## Changes:

### Haven v8.0.0

- #350 [RI    ] Upgrades Haven OpenStack Reference Implementation kops version from 1.18 to 1.19.
- #393 [Haven ] Configures Sonobuoy pre-flight checks with platform specifics
- #396 [Haven ] Adds ARO logging detection
- #406 [Docs  ] Fixes layout of code blocks in docs
- #407 [Addons] Removes Postgres Operator from legacy addons
- #397 [Addons] Adds Postgres Operator to addons with Haven CLI
- #394 [Haven ] Adds OpenShift platform detection to Haven Compliancy Checker
- #374 [RI    ] Fixes protokube multicluster in single project bug
- #389 [Haven ] Updates check wit new master labels
- #384 [Docs  ] Fixes layout of Checks
- #369 [Haven ] Show Checks rationale on the website using single source of truth
- #371 [Docs  ] Updates docs / README's with latest insights
- #373 [Docs  ] Fixes docs images/code blocks on the website

### Haven v7.0.0

- #351 [Haven ] Display error when not able to parse Kubernetes configuration
- #352 [Haven ] Adds JSON output to the CLI and moves logging to top level
- #287 [Addons] Improve Haven dashboard UX
- #357 [Haven ] Adds rationale output for Compliancy Checks
- #353 [Docs  ] Adds new Haven video to online docs
- #371 [Docs  ] Update docs/READMEs with latest insights
- #369 [Docs  ] Add rationale of checks to documentation page

### Haven v6.0.0

- #304 [Chore ] Shuffle: re-aligns the repository layout and documentation with the state of Haven
- #309 [Haven ] Adds CIS as an opt-in external suggested check

### Haven v5.1.0

- #332 [Haven ] Updates compliancy checks like whitelisting GKE SSL automation
- #318 [Haven ] Fixes the external ip check on GKE (response empty vs none)
- #283 [Addons] Initial work on OPA gatekeeper
- #300 [Haven ] Updates compliancy checks like whitelisting OpenShift SSL automation
- #308 [Haven ] Adds separate suggestions table to Haven Compliancy Checker output
- #280 [Docs  ] Improved Azure documentation including Terraform example (Azure AD, Azure CNI, Terraform)
- #234 [Docs  ] Add Amazon EKS documentation
- #302 [Haven ] Fixed naming of 'addons' (used in multiple contexts), suggest instead of require Haven Dashboard
- #270 [Addons] Helm chart for NetworkPolicy, LimitRange and ResourceQuota in namespace
- #288 [Addons] Add cert-manager, ingress-nginx and monitoring to Haven CLI addons

### Haven v5.0.1
- #293 Fix link to oauth2-proxy chart as it is deprecated in the stable repository
- #291 Fix link to Dex Helm chart as it is deprecated in the stable repository

### Haven v5.0.0
- #277 Improved AKS Compliancy checks
- #161 Addons installable from the Haven CLI
- #273 Automated build and deployment of new releases

### Haven v4.4
- #249  Added a check to Haven Compliancy Checker confirming Kubernetes version is up to date
- #267  Removed the Haven Operator in line with documentation using Flux
- #263  Added a check to Haven Compliancy Checker confirming LoadBalancer service type
- #254  Added AppArmor as an alternative to the SELinux/Grsecurity/LKRG check
- #248  Added documentation about deploying a Web Application Firewall (WAF) in your Haven cluster
- #255  Restructured Haven Compliancy Checker categories e.g. clustered addons into single category
- #262  Clarified documentation regarding e.g. Haven Compliancy start vs finish and database usage

### Haven v4.3
- #123  Upgrade Haven with Kops 1.18.2, fixing kops-controller reauth + Octavia cidr whitelisting and replacing our /v2.0/v2.0 octavia patches
- #252  Fixed incorrect number of master nodes HCC check in certain cases
- #247  Fixed Prometheus volume cleanup retention
- #232  Refactored some Haven Compliancy Checkes through user feedback
- #230  Removes sniStrict settings because it does not work with default certificate
- #224  Automatically enforces HSTS on all ingress resources
- #141  Tunes SSL settings of Traefik addon

### Haven v4.2
- #202  Adds Monitoring addon replacing logging + metrics + netchecker addons, switched from ElasticSearch to Loki
- #160  Adds proper cleanup when HCC is interrupted during CNCF checks
- #216  Adds LKRG next to SELinux and Grsec HCC check
- #209  Fixes etcd-manager backup failures upstream (gophercloud reauth) preparing for Kops 1.18 upgrade to solve this
- #188  Improves OpenID token refresh workflow

### Haven v4.1
- #213  Make Haven CLI more compatible
- #219  Upgrade cert-manager to 0.16.1
- #215  Compliancy checker: change AppStore check to Haven Dashboard
- #190  Compliancy checker: Fix correct master and worker node detection on AWS EKS, Azure AKS and Google GKE
- #189  Make it easy to run Haven Compliancy Checker outside Haven Docker image / Add documentation page
- #206  Document setting up Azure AKS
- #173  Upgrade Haven with Kops 1.17.1

### Haven v4.0
- #183  Fixes CSI to run on master nodes
- #163  Adds resource limits to all addons
- #178  Adds Haven Dashboard
- #168  Adds High Availability to Traefik
- #35   Adds support for multiple node pools
- #143  Fixes iptables legacy mode host OS compatibility
- #147  Adds Flux operator (includes #152, #153 and #174)
- #131  Adds Netchecker addon
- #70   Adds Kubernetes Dashboard addon
- #130  Adds ElasticSearch cleanup job using Curator
- #72   Adds online Haven Documentation pages at https://haven.commonground.nl/ (includes #154, #156 and #198)
- #128  Adds Postgres Operator addon

### Haven v3.3
- #127  Adds more HCC checks (mostly HA related).

### Haven v3.2
- #79   Adds more HCC checks including CNCF (integrating sonobuoy).
- #110  Upgrades Devstack leveraging Virtualbox VT-x.

### Haven v3.1
- #106  Upgrades kops version from 1.17a4 to 1.17b1.
- #109  Fixes traefik loadbalancer name uniqueness with multiple clusters in a single project.

### Haven v3.0
- #77   [Major change] Add 'HCC' (Haven Compliancy Checker) checks: part 1.
- #82   [Major and breaking change] Replaces Kubespray with Kops as "Referentie Implementatie" to deploy and maintain clusters.
- #97   Adds usage of external Cloud Controller Manager and Cinder provisioner.
- #96   Adds Customizable master and node volume sizes.
- #95   Adds in-cluster LB support for Octavia CIDR and fixes gophercloud.
- #94   Adds API LB support for Octavia CIDR and fixes gophercloud.
- #91   Upgrades Helm from 2.x to 3.x.
- #84   Bash completion.
- #85   Refactors secrets management to not use a single binary blob anymore.

### Haven v2.3
- #45   Kops PoC succesful.

### Haven v2.2
- #58   Adds OpenID Connect based user management.

### Haven v2.1
- #67   Adds user management to the ElasticSearch stack.
- #65   Adds metrics via Prometheus and Grafana.

### Haven v2.0
- #17   Adds monitoring through Logging (ElasticSearch, Kibana, Fluentd) and Alerting (ElastAlert).

### Haven v1.5
- #63   Fixes Weave CNI on CoreOS networking bug.

### Haven v1.4
- #61   Adds improved kube-controller-manager patch.

### Haven v1.3
- #31   Adds horizontal pod autoscaling.
- #27   Adds rolling cluster upgrades.

### Haven v1.2
- #46   Fixes first users issues (envs location, haven.sh location, docs, env handover, layout).
- #48   Adds automatic publication of Haven docker images.
- #51   Fixes $HOME to equal WORKDIR in Haven container.

### Haven v1.1
- #43   Fixes 'clear security group rules' issue of kube-controller-manager.
- #50   Fixes terraform 0.12.x kubespray incompatibility.
- #12   Adds initial gitlab runners setup.

### Haven v1.0
- #23   Adds public dashboard.
- #42   Adds infra update script.
- #40   Adds octavia lbaas support.
- #38   Adds improved deployments (R2 cinder, kubedb, fixes).
- #36   Adds environment gpg encryption at rest.
- #32   Adds persistent volume support (RWO).
- #29   Adds kubernetes user management.
- #34   Adds metrics-server.
- #26   Adds traefik automated SSL deployments.
- #25   Adds helm deployments.
- #7    Proof of Concept Haven: Kubernetes cluster on OpenStack, private topology with bastion/lbaas, RBAC enabled.
